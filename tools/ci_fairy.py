#!/usr/bin/env python3

import boto3
import botocore
import click
import collections
import colored
import datetime
import functools
import gitdb
import gitlab
from gitlab import Gitlab  # type:ignore
import hashlib
import importlib
import io
import itertools
import jinja2
import json
import fnmatch
import logging
import os
import re
import requests
import shutil
import sys
import time
import yaml
from botocore.client import Config
from copy import deepcopy
from requests.adapters import HTTPAdapter, Retry
from pathlib import Path


fdo_s3_default_expiration = [
    ("https://s3.freedesktop.org/artifacts", "4w"),
    ("https://s3.freedesktop.org/mesa-lava", "4w"),
]


class _env_ci_fairy:
    context = {}

    @staticmethod
    def hashfiles(*argv):
        """
        Hashes the filenames (@argv) and the content of the files in some
        unspecified (but stable) way and returns a 64 characters (hex) digest.
        """

        import hashlib

        argv = list(argv)

        if not argv:
            raise ValueError("must specify at least one filename to hash")

        h = hashlib.new("sha256")
        h.update(f"{len(argv)}\n".encode("utf-8"))
        for filename in argv:
            with open(filename, "rb") as f:
                d = f.read()
                h.update(f"{len(filename)} {filename}\n{len(d)}\n".encode("utf-8"))
                h.update(d)
        return h.hexdigest()

    @staticmethod
    def sha256sum(path, prefix=True):
        """
        Hashes content of the files with sha256 and produces a string in the
        form "sha256-<hexdigest>".

        If prefix is False, the returned string is only the hexdigest.
        """

        import hashlib

        h = hashlib.new("sha256")
        with open(path, "rb") as f:
            d = f.read()
            h.update(d)
        return f'{"sha256-" if prefix else ""}{h.hexdigest()}'

    @staticmethod
    def import_module(module_name):
        """
        Allows to import a python module directly in the template.

        Example:
        {% set time = ci_fairy.import_module( 'time' ) %}
        {{ time.time() }}
        """
        return importlib.import_module(module_name)


def gitlab_project(gitlab, project=None, fallback="__default__"):
    """
    Returns the Gitlab Project object for the given project name. Where name
    is None, it is taken from the CI environment.
    """
    if project is None:
        # we can't default fallback to os.getenv() without breaking the
        # tests (which rely on putenv), so let's use a special value here.
        if fallback == "__default__":
            fallback = os.getenv("CI_PROJECT_ID")
        if fallback is None:
            return None
        p = gitlab.projects.get(fallback)
    else:
        p = gitlab.projects.list(search=project, search_namespaces=True)
        # gitlab does substring matches, so foo/bar matches foo/barbaz as
        # well
        if len(p) > 1:
            p = [x for x in p if x.path == project or x.path_with_namespace == project]
        p = p[0] if len(p) == 1 else None

    return p


class ColorFormatter(logging.Formatter):
    def format(self, record):
        COLORS = {
            "DEBUG": colored.fg("orchid"),
            "INFO": colored.attr("reset"),
            "WARNING": colored.fg("salmon_1"),
            "ERROR": colored.fg("red"),
            "CRITICAL": colored.fg("red") + colored.bg("yellow"),
        }
        return COLORS[record.levelname] + super().format(record) + colored.attr("reset")


log_format = "%(levelname)s: %(message)s"
logger_handler = logging.StreamHandler()
if os.isatty(sys.stderr.fileno()):
    logger_handler.setFormatter(ColorFormatter(log_format))
else:
    logger_handler.setFormatter(logging.Formatter(log_format))
logger = logging.getLogger("ci-fairy")
logger.addHandler(logger_handler)
logger.setLevel(logging.ERROR)


def make_junit_xml(filename, test_suite_id, test_suite_name, failures):
    """
    Write a JUNIT XML compatible file to filename based on the failures
    dictionary where each failure is a tuple of (id, short-message,
    full-message).
    """
    import xml.etree.cElementTree as ET

    root = ET.Element("testsuites")
    suite = ET.SubElement(
        root,
        "testsuite",
        id=test_suite_id,
        name=test_suite_name,
        tests=str(len(failures)),
        errors=str(len(failures)),
    )
    for f in failures:
        tcaseid, name, message = f
        tcaseid = f"{test_suite_id}.{tcaseid}"
        tcase = ET.SubElement(suite, "testcase", id=tcaseid, name=name)
        failure = ET.SubElement(tcase, "failure", message=name, type="ERROR")
        failure.text = message

    ET.ElementTree(root).write(filename, encoding="UTF-8", xml_declaration=True)


class LazyGitlab(object):
    def __init__(self):
        self.initialized = False
        self.url = None
        self.job_token = None
        self.private_token = None
        self.gitlab = None

    def __getattribute__(self, name):
        if name == "gitlab" and not self.initialized:
            tokens = {}
            if self.private_token is not None:
                logger.debug("Using authfile")
                tokens["private_token"] = self.private_token
            elif self.job_token is not None:
                logger.debug("Using $CI_JOB_TOKEN")
                tokens["job_token"] = self.job_token
            else:
                logger.debug("Connecting without authentication")

            if not self.url:
                raise click.UsageError("Missing gitlab URL")

            self.gitlab = Gitlab(self.url, **tokens)
            self.initialized = True
            return self.gitlab

        return object.__getattribute__(self, name)


class GitCommitValidator(object):
    def __init__(self, commit):
        self.commit = commit
        self.errors = []

    def _check_author_email(self):
        # The full address is @users.noreply.gitlab.freedesktop.org but
        # let's make this more generic
        if "@users.noreply" in self.commit.author.email:
            self.__error(
                "git author email invalid\n"
                "Please set your name and email with the commands\n"
                "    git config --global user.name Your Name\n"
                "    git config --global user.email your.email@provider.com\n"
            )

    def _check_fixup_squash(self):
        if self.commit.message.startswith("fixup!") or self.commit.message.startswith(
            "squash!"
        ):
            self.__error('Leftover "fixup!" or "squash!" commit found, please squash')

    def _check_second_line(self):
        try:
            second_line = self.commit.message.split("\n")[1]
            if second_line != "":
                self.__error("Second line in commit message must be empty")
        except IndexError:
            pass

    def check(self):
        self._check_author_email()
        self._check_fixup_squash()
        self._check_second_line()

    def check_gpg_signed(self, must_be_signed):
        if not must_be_signed:
            if self.commit.gpgsig:
                self.__error("Do not GPG sign commits")
        else:
            if not self.commit.gpgsig:
                self.__error("Commit is not GPG signed")

    def check_sob(self, must_have_sob):
        lines = self.commit.message.split("\n")
        sob = [l for l in lines if l.startswith("Signed-off-by:")]

        if not must_have_sob:
            if sob:
                self.__error("Do not use Signed-off-by in commits")
        else:
            if not sob:
                self.__error('Missing "Signed-off-by: author information"')

    def check_text_width(self, tw):
        lines = self.commit.message.split("\n")
        shortlog = lines[0]
        if len(shortlog) >= tw:
            is_revert = (
                shortlog.startswith('Revert "')
                and "This reverts commit" in self.commit.message
            )
            if not is_revert:
                self.__error(f"Commit message subject must not exceed {tw} characters")

    def _match_pattern_rule(self, subject, rule):
        if rule.get("where", "message") == "subject":
            msg = subject
        else:
            msg = self.commit.message

        regex = str(rule["regex"])
        return re.search(regex, msg, re.MULTILINE)

    def check_patterns(self, rules):
        subject = self.commit.message.split("\n")[0]
        patterns = rules.get("patterns", {})

        for rule in patterns.get("require", []):
            if self._match_pattern_rule(subject, rule) is None:
                self.__error(
                    rule.get("message", f'Required pattern not found: {rule["regex"]}')
                )

        for rule in patterns.get("deny", []):
            if self._match_pattern_rule(subject, rule) is not None:
                self.__error(
                    rule.get("message", f'Disallowed pattern found: {rule["regex"]}')
                )

    @property
    def failed(self):
        return bool(self.errors)

    def __error(self, errormsg):
        self.errors.append(errormsg)

    @property
    def error(self):
        if not self.failed:
            return None

        msg = (
            f"Commit message check failed\n\n"
            f"  commit: {str(self.commit)}\n"
            f"  author: {self.commit.author.name} <{self.commit.author.email}>\n"
            f"\n"
            f"  {self.commit.summary}\n"
            f"\n"
            f"\n"
            f"After correcting the issues below, force-push to the same branch.\n"
            f"This will re-trigger the CI.\n\n"
            f"---\n"
        )
        for idx, err in enumerate(self.errors):
            msg += f"{idx+1}. {err}\n---\n"
        return msg

    @property
    def junit_tuple(self):
        """
        A tuple of (id, short message, full description)
        """
        if not self.failed:
            return None

        msg = "\n\n".join([f"{idx + 1}. {err}" for idx, err in enumerate(self.errors)])

        return (
            str(self.commit),
            f"{str(self.commit)[:8]} failed {len(self.errors)} commit message checks",
            msg,
        )


class S3(object):
    """
    An abstract object providing a common API
    for AWS S3 server (remotes) or local file system
    """

    def __init__(self):
        pass

    @classmethod
    def s3(cls, full_path, credentials=None):
        """
        Factory method to get an S3 object based on
        its path.
        :param full_path: the full path of the object (local posix path or "minio://host/bucket/key")
        :type full_path: str
        :param credentials: a file path with the appropriate credentials (access key, secret key and session token)
        :type credentials: str

        :returns: An S3Object
        """
        prefix = "minio://"

        if not full_path.startswith(prefix):
            return S3Object(full_path)

        # full_path should now be in the form `minio://host/bucket/key`

        path_str = full_path[len(prefix) :]

        s3_remote = S3Remote(path_str, credentials)

        return s3_remote.get(path_str)


class S3Object(object):
    """
    Wrapper around the S3 API for local or remote objects.
    A plain S3Object wraps a local file system.

    Subclasses wrap the remote API.
    """

    def __init__(self, key):
        self.key = key
        self.path = Path(key)

    @property
    def exists(self):
        return self.path.exists()

    @property
    def is_dir(self):
        if not self.exists:
            return self.key.endswith("/")

        return self.path.is_dir()

    @property
    def is_local(self):
        return True

    @property
    def name(self):
        return self.path.name

    def copy_from(self, other):
        if other.is_local:
            try:
                shutil.copy(other.key, self.path)
            except IsADirectoryError:
                raise IsADirectoryError(
                    f"Error: cannot do recursive cp of directory '{other.key}'"
                )
        else:
            other.download_file(self)

    @property
    def children(self):
        return [S3Object(p) for p in self.path.glob("*")]


class S3Remote(S3Object):
    """
    A remote S3 server.
    This contains the list of buckets.
    """

    def __init__(self, full_path, credentials):
        super().__init__("/")

        with open(credentials) as credfile:
            data = json.load(credfile)

        host = full_path

        if "/" in full_path:
            host, _ = full_path.split("/", 1)

        creds = None

        for h, c in data.items():
            if h == host:
                creds = c
                break

        if not host:
            raise KeyError(f"missing host information in 'minio://{full_path}'")

        if creds is None:
            raise KeyError(
                f"host '{host}' not found in credentials, please use 'ci-fairy minio login' first"
            )

        self._s3 = boto3.resource(
            "s3",
            endpoint_url=creds["endpoint_url"],
            aws_access_key_id=creds["AccessKeyId"],
            aws_secret_access_key=creds["SecretAccessKey"],
            aws_session_token=creds["SessionToken"],
            config=Config(signature_version="s3v4"),
            region_name="us-east-1",
        )
        self._name = host

        self._buckets = None

    @property
    def buckets(self):
        if self._buckets is None:
            bucket_names = [b.name for b in self._s3.buckets.all()]
            self._buckets = {b: self._s3.Bucket(b) for b in bucket_names}
        return self._buckets

    @property
    def name(self):
        return self._name

    @property
    def is_local(self):
        return False

    @property
    def exists(self):
        return True

    @property
    def is_dir(self):
        return True

    def copy_from(self, other):
        raise ValueError("No destination bucket provided")

    @property
    def children(self):
        return [S3Bucket(b, self) for b in self.buckets.values()]

    def get(self, path):
        # Remove the host from the URL
        path = path[len(self.name) :].lstrip("/")

        if not path:
            # minio://host/
            return self

        bucket_name = path
        key = ""

        if "/" in path:
            bucket_name, key = path.split("/", 1)

        return S3Bucket(self._s3.Bucket(bucket_name), self).get(key)


class S3Bucket(S3Object):
    """
    A remote S3 bucket
    """

    def __init__(self, bucket, remote):
        super().__init__("/")
        self._bucket = bucket
        self._remote = remote

    @property
    def exists(self):
        return True

    @property
    def is_dir(self):
        return True

    @property
    def name(self):
        return self._bucket.name

    @property
    def children(self):
        if self.name not in self._remote.buckets:
            # minio://host/bucket_that_doesn_t_exist
            raise FileNotFoundError(
                f"bucket '{self.name}' doesn't exist on {self._remote.name}"
            )

        objs = [o.key for o in self._bucket.objects.all()]
        children = []

        for o in objs:
            if "/" in o:
                # minio://host/bucket/some/path/some/file
                # o is now: some/path/some/file
                root, _ = o.split("/", 1)
                if root not in children:
                    # root is "some" and is not in the children list
                    children.append(root)
            else:
                # minio://host/bucket/some_file
                children.append(o)

        return [S3RemoteObject(self, c) for c in children]

    def copy_from(self, other):
        dst = other.name
        self.upload_file(other, dst)

    def get(self, key):
        if not key:
            # - minio://host/bucket
            # - minio://host/bucket/
            return self

        return S3RemoteObject(self, key)

    @property
    def objects(self):
        return self._bucket.objects.all()

    def upload_file(self, local_obj, remote_obj):
        if not local_obj.is_local:
            raise ValueError("at least one argument must be a local path")

        return self._bucket.upload_file(str(local_obj.path), str(remote_obj))

    def _download_file(self, remote_obj, local_obj):
        if remote_obj.is_dir:
            raise IsADirectoryError("cannot do recursive cp")

        local_path = local_obj.path

        if local_obj.is_dir:
            if not local_obj.exists:
                raise IsADirectoryError(f"directory '{local_path}' does not exist")

            local_path = local_path / remote_obj.name

        return self._bucket.download_file(str(remote_obj.key), str(local_path))

    def download_file(self, local_dest):
        return self._download_file(self, local_dest)


class S3RemoteObject(S3Object):
    """
    A remote S3 object.
    The key is the full path of the file or directory within
    its bucket.
    """

    def __init__(self, bucket, key):
        super().__init__(key)
        self._bucket = bucket
        self._is_dir = key.endswith("/")
        self._children = None
        self._exists = False

    @property
    def is_dir(self):
        return self._is_dir

    @property
    def is_local(self):
        return False

    @property
    def exists(self):
        return self._exists

    @property
    def children(self):
        if self._children is None:
            self._children = []
            objs = [o.key for o in self._bucket.objects if o.key.startswith(self.key)]

            # find if the object exists already (full key matches)
            self._exists = self.key in objs

            # special case for directories
            if not objs:
                # nothing in the remote matches, check if we
                # have a terminating '/'
                self._is_dir = self.key.endswith("/")

            elif not self._exists:
                # at least one remote object starts with our key,
                # check if we have a parent of a remote object, or
                # just if the path and name starts with the same key

                # build the list of all parents of all objects
                parents = [p for o in objs for p in Path(o).parents]

                self._is_dir = self._exists = self.path in parents

                # compute the list of files or dir immediately below the
                # current dir
                if self._is_dir:
                    for o in objs:
                        path = Path(o)

                        for parent in path.parents:
                            if parent == self.path:
                                self._children.append(path)
                                break

                            path = parent
        return self._children

    def copy_from(self, other):
        dst = self.path

        if self.is_dir:
            dst = self.path / other.name

        self._bucket.upload_file(other, dst)

    def download_file(self, local_dest):
        return self._bucket._download_file(self, local_dest)


@click.group()
@click.option("-v", "--verbose", count=True, help="increase verbosity")
@click.option(
    "--gitlab-url",
    help="GitLab URL with transport protocol, e.g.  https://gitlab.freedesktop.org",
)
@click.option(
    "--authfile", help="Path to a file containing the gitlab auth token string"
)
@click.pass_context
def ci_fairy(ctx, verbose, gitlab_url, authfile):
    verbose_levels = {
        0: logging.ERROR,
        1: logging.INFO,
        2: logging.DEBUG,
    }
    logger.setLevel(verbose_levels.get(verbose, 0))

    if gitlab_url is None:
        gitlab_url = os.getenv("CI_SERVER_URL")
        if not gitlab_url:
            import git

            try:
                from urllib.parse import urlparse

                repo = git.Repo(search_parent_directories=True)
                url = repo.remotes.origin.url
                # urlparse doesn't work with ssh specifiers which are in the
                # form user@host:path. Convert those into ssh://user@host so
                # urlparse works.
                if "//" not in url[:10]:
                    url = "ssh://{}" + url

                # split off the user@ component if it's there
                server = urlparse(url).netloc.split("@")[-1]
                # split off an ssh-like path component if it's there
                server = server.split(":")[0]
                # Force https because what else could it be
                gitlab_url = "https://" + server
            except git.exc.InvalidGitRepositoryError:
                pass
            except AttributeError:  # origin does not exist
                pass

    ctx.ensure_object(LazyGitlab)
    ctx.obj.url = gitlab_url

    if authfile is not None:
        token = open(authfile).read().strip()
        ctx.obj.private_token = token
    else:
        token = os.getenv("CI_JOB_TOKEN")
        if token:
            ctx.obj.job_token = token


def credentials_option(required=True):
    def inner_function(func):
        @click.option(
            "--credentials",
            default=".minio_credentials",
            help="the file to store the credentials (default to $PWD/.minio_credentials)",
            type=click.Path(
                exists=required,
                file_okay=True,
                dir_okay=False,
                writable=not required,  # we write the file if required is false (login case)
                readable=True,
                allow_dash=False,
            ),
        )
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return inner_function


def token_file_option():
    def inner_function(func):
        @click.option(
            "--token-file",
            help="a file to read the token from",
            type=click.Path(
                exists=True,
                file_okay=True,
                dir_okay=False,
                writable=False,
                readable=True,
                allow_dash=False,
            ),
        )
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return inner_function


@ci_fairy.group()
def minio():
    pass


@minio.command()
@credentials_option(required=False)
@click.option(
    "--endpoint-url",
    default="https://minio-packet.freedesktop.org",
    help="The minio instance to contact",
)
@token_file_option()
@click.argument("token", default="")
def login(credentials, endpoint_url, token, token_file):
    """Login to the minio server"""
    from urllib.parse import urlparse

    credentials = Path(credentials)

    if token_file:
        token = open(token_file).read()

    if not token:
        logger.error("A token is required")
        sys.exit(2)

    data = {}
    if credentials.exists():
        with open(credentials) as infile:
            data = json.load(infile)

    session = boto3.Session()

    sts = session.client(
        "sts",
        endpoint_url=endpoint_url,
        config=Config(signature_version="s3v4"),
        region_name="us-east-1",
    )

    roleArn = "arn:aws:iam::123456789012:role/FederatedWebIdentityRole"
    ret = sts.assume_role_with_web_identity(
        DurationSeconds=900,
        WebIdentityToken=token,
        RoleArn=roleArn,
        RoleSessionName="session_name",
    )

    server = urlparse(endpoint_url).netloc
    server = server.strip("/")
    creds = ret["Credentials"]
    creds["endpoint_url"] = endpoint_url
    creds["Expiration"] = creds["Expiration"].isoformat()
    data[server] = creds
    with open(credentials, "w") as outfile:
        json.dump(data, outfile)


@minio.command()
@credentials_option()
@click.argument("path", default=".")
@click.pass_context
def ls(ctx, credentials, path):
    try:
        s3_obj = S3.s3(path, credentials)
    except KeyError as e:
        ctx.fail(e)
    except botocore.exceptions.ClientError as e:
        ctx.fail(e)

    # for ls, we need to actually query the object to check if it exists
    # we can not rely on the assumption it does exist
    # calling children lazily evaluate the object and we sync ourself
    # with the remote
    try:
        children = s3_obj.children
    except FileNotFoundError as e:
        ctx.fail(e)

    if not s3_obj.exists:
        ctx.fail(f"file '{path}' does not exist")

    if not s3_obj.is_dir:
        print(s3_obj.name)
    else:
        for o in children:
            print(o.name)


@minio.command()
@credentials_option()
@click.argument("src")
@click.argument("dst")
@click.pass_context
def cp(ctx, credentials, src, dst):
    try:
        src = S3.s3(src, credentials)
    except KeyError as e:
        ctx.fail(e)

    # src doesn't exist
    if not src.exists and src.is_local:
        ctx.fail(f"source file '{src.path}' does not exist")

    try:
        dst = S3.s3(dst, credentials)
    except KeyError as e:
        ctx.fail(e)

    try:
        dst.copy_from(src)
    except ValueError as e:
        ctx.fail(e)
    except FileNotFoundError as e:
        ctx.fail(e)
    except IsADirectoryError as e:
        ctx.fail(e)
    except boto3.exceptions.S3UploadFailedError as e:
        ctx.fail(e)
    except botocore.exceptions.ClientError as e:
        ctx.fail(e)


@ci_fairy.command()
@token_file_option()
@click.option(
    "--token",
    default=os.getenv("CI_JOB_JWT"),
    help="The JWT token used to authenticate, defaults to environment variable $CI_JOB_JWT",
)
@click.option(
    "--new-lines",
    is_flag=True,
    help="When using progress bars, do not overwrite the current bar but instead put every update on a new line. This is useful when teh CI system relies on new lines to detect if a job is stuck.",
)
@click.option(
    "--expires",
    default=None,
    help="The number of days/weeks from today at which the object is no longer cacheable. In the form of `Nd` for N days, or `Nw` for N weeks",
)
@click.argument("src")
@click.argument("dst")
@click.pass_context
def s3cp(ctx, token, token_file, src, dst, new_lines, expires):
    """
    basically a wrapper around (when dst is an s3 endpoint):
    `curl  -H "Authorization: Bearer $TOKEN" \
           -H "x-amz-acl: public-read-write" \
           -H "Content-Type: application/octet-stream" \
           -T src dst`

    src and dst can either be a remote endpoint but we need one remote URI
    and one local file.

    Copy files between a local system and an S3 endpoint based on the specified source and destination.
    When SRC is a local file, the command uploads the local file specified in SRC to the remote DST endpoint.
    When DST is a local file, the command downloads the file from the remote SRC endpoint to the local file specified in DST.

    Use '-' as dst to print the file to stdout.
    """

    def is_remote(uri):
        return uri.startswith("http://") or uri.startswith("https://")

    def validate(response, status_code=200):
        if response.status_code != status_code:
            ctx.fail(f"error {response.status_code}: {response.content}")

    if is_remote(src) and is_remote(dst):
        raise click.UsageError("src and dst can not be both remote URLs")
    if not is_remote(src) and not is_remote(dst):
        raise click.UsageError("src and dst can not be both local paths")

    if token_file:
        with open(token_file) as f:
            token = f.read().strip()

    headers = {
        "x-amz-acl": "public-read-write",
        "Content-Type": "application/octet-stream",
    }

    if token:
        headers["Authorization"] = f"Bearer {token}"

    # http setup
    attempts = 3
    retries = Retry(
        backoff_factor=30,
        connect=attempts,
        read=attempts,
        redirect=attempts,
        status_forcelist=[429, 500, 502, 503, 504],
        raise_on_redirect=False,
    )
    session = requests.Session()
    adapter = HTTPAdapter(max_retries=retries)
    for protocol in ["http://", "https://"]:
        session.mount(protocol, adapter)

    src_path = Path(src)
    dst_path = Path(dst)

    if is_remote(src):
        # Downloading to a target directory
        if dst_path.is_dir():
            dst_path = dst_path / src_path.name
        elif dst.endswith("/"):
            ctx.fail(f"destination directory '{dst}' does not exist")
        # Downloading to a target file, the parent directory needs to exist
        elif not dst_path.parent.is_dir():
            ctx.fail(f"destination directory '{dst_path.parent}' does not exist")

        r = session.get(src, headers=headers, stream=True)
        validate(r)

        if dst == "-":
            for chunk in r.iter_content(chunk_size=128):
                sys.stdout.buffer.write(chunk)
        else:
            with open(dst_path, "wb") as fd:
                for chunk in r.iter_content(chunk_size=128):
                    fd.write(chunk)
    else:
        if not src_path.exists():
            ctx.fail(f"source file '{src}' does not exist")
        if src_path.is_dir():
            ctx.fail(f"source file '{src}' is a directory")
        if dst.endswith("/"):
            ctx.fail(f"destination file '{dst}' is a directory")

        # special case for freedesktop: we want some special expirations on some buckets:
        for expiration_bucket, exp in fdo_s3_default_expiration:
            if dst.startswith(expiration_bucket) and expires is None:
                expires = exp

        if expires is not None:
            regex = re.compile(r"(\d+)([dw])")
            g = regex.match(expires)
            if not g:
                ctx.fail(
                    f"invalid --expires parameter, expected the following regex: '{regex.pattern}'"
                )
            n = int(g.group(1))
            days = n if g.group(2) == "d" else 0
            weeks = n if g.group(2) == "w" else 0
            expiration_date = datetime.datetime.now() + datetime.timedelta(
                days=days, weeks=weeks
            )
            headers["Expires"] = expiration_date.ctime()

        file_size = src_path.stat().st_size

        if file_size <= 90 * 1024 * 1024:
            with open(src, "rb") as f:
                r = session.put(dst, data=f, headers=headers)
        else:
            # multi-part upload
            r = session.post(dst, headers=headers, params={"uploads": ""})
            validate(r)

            # ideally we want to use xmltodict but given that it is one more dependency
            # write a custom parser
            if b"UploadId" not in r.content:
                ctx.fail(f"can not initiate multi-part upload: {r.content}")

            _, key, _ = r.content.split(b"UploadId")
            key = key.strip(b"></").decode("utf-8")

            sys.stdout.write(
                "  0.00% ( 0)",
            )
            part_number = 0
            chunk = 10 * 1024 * 1024
            parts = {}
            sent = 0
            completed = False
            sep = "\r" if not new_lines else "\n"

            with open(src_path, "rb") as f:
                try:
                    while True:
                        piece = f.read(chunk)
                        if piece == b"":
                            completed = True
                            break
                        part_number += 1
                        md5 = hashlib.md5(piece).hexdigest()
                        resp = session.put(
                            dst,
                            headers=headers,
                            params={"uploadId": key, "partNumber": part_number},
                            data=piece,
                        )
                        validate(resp)
                        sent += len(piece)
                        sys.stdout.write(
                            f"{sep}{100 * sent / file_size: 6.2f}% ({part_number: 2})",
                        )
                        etag = resp.headers["etag"]
                        if etag.strip('"') != md5:
                            break
                        parts[part_number] = etag
                except KeyboardInterrupt:
                    sys.stdout.write(
                        f"\nAborting transfer",
                    )

            sys.stdout.write("\n")

            complete = """<?xml version="1.0" encoding="UTF-8"?><CompleteMultipartUpload xmlns="http://s3.amazonaws.com/doc/2006-03-01/">\n"""
            for i in parts:
                complete += f"""<Part>\n <PartNumber>{i}</PartNumber>\n <ETag>{parts[i]}</ETag></Part>\n"""
            complete += """</CompleteMultipartUpload>"""

            del headers["Content-Type"]

            if completed:
                r = session.post(
                    dst, headers=headers, params={"uploadId": key}, data=complete
                )
            else:
                r = session.delete(dst, headers=headers, params={"uploadID": key})
                validate(r, 204)
                return

    validate(r)


@ci_fairy.command()
@click.option(
    "--repository", help="The registry repository to work on, e.g.  fedora/latest"
)
@click.option("--project", help="Project name, e.g.  freedesktop/ci-templates")
@click.option("--all", is_flag=True, help="delete all images")
@click.option("--exclude-tag", help="fnmatch pattern to exclude, i.e. to not delete")
@click.option("--tag", help="fnmatch pattern to delete")
@click.option("--dry-run", is_flag=True, help="Don't actually delete anything")
@click.pass_context
def delete_image(ctx, repository, project, all, exclude_tag, tag, dry_run):
    """
    Delete images from the container registry.

    One of --tag, --exclude-tag or --all is required.

    Use --tag to delete a single tag only, use --exclude-tag to delete all
    but the given tag. Use --all to delete all images.

    Use with --repository to limit to one repository only.

    The tool is designed to use the GitLab environment variables for user,
    passwords, projects, etc. where possible.

    Where other authentication is needed, use a --authfile containing the
    string that is your gitlab private token value with 'api' access.
    This tool will strip any whitespaces from that file and use the rest of
    the file as token value.
    """
    if not tag and not exclude_tag and not all:
        raise click.UsageError("One of --tag, --exclude-tag, or --all is required.")

    p = gitlab_project(ctx.obj.gitlab, project)
    if not p:
        raise click.BadParameter("Unable to find or identify project")

    repos = [
        r for r in p.repositories.list() if repository is None or repository == r.name
    ]
    for repo in repos:
        logger.debug(f"Repository {repo.name}")
        for t in repo.tags.list(all=True):
            if all:
                do_delete = True
            elif fnmatch.fnmatchcase(t.name, exclude_tag or "\0"):
                do_delete = False
            elif fnmatch.fnmatchcase(t.name, tag or "\0"):
                do_delete = True
            elif exclude_tag:
                do_delete = True
            else:
                do_delete = False

            if do_delete:
                logger.info(f"Deleting tag {repo.name}:{t.name}")
                if not dry_run:
                    try:
                        t.delete()
                    except gitlab.exceptions.GitlabDeleteError as e:
                        if e.response_code == 403:
                            raise click.BadParameter(
                                f"Insufficient permissions to delete tag {repo.name}:{t.name}"
                            )
                        else:
                            raise e


@ci_fairy.command()
@click.option(
    "--filename", help="The file to run through the linter (default: .gitlab-ci.yml)"
)
@click.option(
    "--project",
    type=str,
    default="freedesktop/ci-templates",
    help="Project name",
    show_default=True,
)
@click.pass_context
def lint(ctx, filename, project: str):
    """
    Run the .gitlab-ci.yml through the gitlab instance's linter.

    If no filename is given, this tool searches for a .gitlab-ci.yml in the
    current path. If none exists, it will search the parent directories for
    one.

    Specifying the project should not usually be necessary - the file content
    is uploaded for linting and thus should be valid for any project.
    """
    if not filename:
        f = Path(".gitlab-ci.yml").resolve()
        if not f.exists():
            for p in f.parents:
                f = Path(p / ".gitlab-ci.yml")
                if f.exists():
                    filename = f
                    break
                # Search upwards but only until within the current git tree
                # (if any)
                if Path(f.parent / ".git").exists():
                    break

            if not filename:
                raise click.UsageError("Unable to find .gitlab-ci.yml")
        else:
            filename = f
    else:
        filename = Path(filename)

    logger.debug(f"linting file {filename}")

    with open(filename) as fd:
        p = ctx.obj.gitlab.projects.get(project)
        if p is None:
            raise click.UsageError(f"Failed to find project {project}")
        lint = p.ci_lint.create({"content": fd.read()})
        if lint.valid:
            logger.info(f"{filename} has no linting errors")
        else:
            logger.error(f"{filename} failed linting:")
            for e in lint.errors:
                logger.error(e)


class YamlError(Exception):
    pass


class ExtendedYaml(collections.UserDict):
    """
    A version of YAML that supports extra keywords.
    Requirements
    ============
    An extended YAML file must be a dictionary.
    Features
    ========
    extends:
    --------
    Supported in: top-level dictionaries
    The ``extends:`` keyword makes the current dictionary inherit all
    members of the extended dictionary, according to the following rules::
    - where the value is a non-empty dict, the base and new dicts are merged
    - where the value is a non-empty list, the base and new list are
      concatinated
    - where the value is an empty dict or empty list, the new value is the
      empty dict/list.
    - otherwise, the new value overwrites the base value.
    Example::
        foo:
          bar: [1, 2]
          baz:
            a: 'a'
            b: 'b'
          bat: 'foobar'
        subfoo:
          extends: bar
          bar: [3, 4]
          baz:
            c: 'c'
          bat: 'subfoobar'
    Results in the effective values for subfoo::
      subfoo:
         bar: [1, 2, 3, 4]
         baz: {a: 'a', b: 'b', c: 'c'}
         bat: 'subfoobar'
      foo:
        bar: 1
    include:
    --------
    Supported in: top-level only
    The ``include:`` keyword includes the specified file at the place. The
    path to the included file is relative to the source file.
    Example::
         # content of firstfile
         foo:
             bar: [1, 2]
         #content of secondfile
         bar:
             baz: [3, 4]
         include: firstfile
         foobar:
           extends: foo
    Not that the included file will work with the normal YAML rules,
    specifically: where the included file has a key with the same name this
    section will be overwritten with the later-defined. The position of the
    ``include`` statement thus matters a lot.
    version:
    --------
    The YAML file version (optional). Handled as attribute on this object and does not
    show up in the dictionary otherwise. This version must be a top-level
    entry in the YAML file in the form "version: 1" or whatever integer
    number and is exposed as the "version" attribute.
    Where other files are included, the version of that file must be
    identical to the first version found in any file.
    :: attribute: version
        An optional attribute with the version number as specified in the
        YAML file(s).
    """

    def __init__(self, include_path=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.include_path = include_path

    def __load(self, stream):
        data = io.StringIO()
        self.__process_includes(stream, dest=data)
        data = yaml.safe_load(data.getvalue())
        if not isinstance(data, dict):
            raise YamlError("Invalid YAML data format, expected a dictionary")

        if data.get("extends"):
            raise YamlError(
                'Invalid section name "extends", this is a reserved keyword'
            )

        data = self.__process_extends(data)
        for k, v in data.items():
            self[k] = v

    def __process_includes(self, source, dest, level=0):
        """
        Handles include: statements. Reads the source line-by-line
        and write it to the destination. Where a ``include: filename`` line
        is present, the filename is opened and this function is called
        recursively for that file.
        """
        if level > 10:  # 10 levels of includes must be enough.
            return ""

        for line in source:
            # version check - all included files must have the same version
            # as the original file
            if line.startswith("version:"):
                version = int(line[len("version:") :].strip())
                try:
                    if self.version != version:
                        raise YamlError(
                            f"Cannot include file {source}, version mismatch"
                        )
                except AttributeError:
                    self.version = version
                continue

            if not line.startswith("include: "):
                dest.write(line)
                continue

            # used for test cases only, really. all uji user cases use a
            # file anyway, not a string.
            if not self.include_path:
                raise YamlError("Cannot include from a text stream")

            filename = line[len("include:") :].strip()
            with open(Path(self.include_path) / filename) as included:
                self.__process_includes(included, dest, level + 1)

    def __process_extends(self, yaml):
        def merge(a, b):
            """
            Helper function to a and b together:
            - Where a and b are lists, the result is a + b
            - Where a and b are dicts, the result is the union of a and b
            - Otherwise, the result is b
            This performs a deep copy of all lists/dicts so the result does what
            you'd expect.
            """
            if type(a) != type(b):
                raise ValueError()

            if isinstance(a, list):
                return a + b
            elif isinstance(a, dict):
                merged = {}
                for k, v in a.items():
                    merged[k] = v
                for k, v in b.items():
                    merged[k] = v
                return merged
            else:
                return b

        # yaml is modified in the loop, so we have to list() it
        for section, data in list(yaml.items()):
            if not isinstance(data, dict):
                continue

            referenced = data.get("extends")
            if not referenced:
                continue

            if referenced not in yaml or referenced == section:
                raise YamlError(f'Invalid section for "extends: {referenced}"')

            # We need deep copies to avoid references to lists within dicts,
            # etc.
            combined = deepcopy(yaml[referenced])
            data = deepcopy(data)
            for item, value in data.items():
                if item == "extends":
                    continue

                try:
                    base = combined[item]
                except KeyError:
                    # base doesn't have this key, so we can just
                    # write it out
                    combined[item] = value
                else:
                    try:
                        combined[item] = merge(base, value)
                    except ValueError:
                        raise YamlError(
                            f"Mismatched types for {item} in {section} vs {referenced}"
                        )

            yaml[section] = combined

        return yaml

    @classmethod
    def load_from_file(cls, filename):
        from pathlib import Path

        path = Path(filename)
        if not path.is_file():
            raise YamlError(f'"{filename}" is not a file')

        with open(path) as f:
            yml = ExtendedYaml(include_path=Path(filename).parent)
            yml.__load(f)
            return yml

    @classmethod
    def load_from_stream(cls, stream):
        yml = ExtendedYaml()
        yml.__load(io.StringIO(stream))
        return yml


class ci_fairyFileSystemLoader(jinja2.FileSystemLoader):
    def get_source(self, environment, template):
        source, filename, uptodate = super().get_source(environment, template)
        return source.replace("🧚", "ci_fairy"), filename, uptodate


@ci_fairy.command()
@click.option("--config", help="YAML configuration file", required=False, multiple=True)
@click.option("--root", help="YAML node to use as root node", type=str, multiple=True)
@click.option("--output-file", "-o", help="output file to write to (default: stdout)")
@click.option(
    "--verify",
    is_flag=True,
    help="Compare the generated file against the existing one and fail if they differ",
)
@click.argument("template", required=False)
@click.pass_context
def generate_template(ctx, config, root, template, output_file, verify):
    """
    Generate a file based on the given Jinja2 template and the YAML
    configuration file.

    If called without any arguments, this command reads the
    .gitlab-ci/config.yml and .gitlab-ci/ci.template files and (over)writes
    the .gitlab-ci.yml file.

    Otherwise, both --config and the template must be provided on the
    commandline.

    Where the --root option is specified, the children of that root node are
    the data to be used in the template. For nested root nodes, use
    /path/to/node.

    Where the --verify option is given, the output file is not overwritten.
    Instead, ci-fairy compares the newly generated to the existing output
    file and exits with an error if the two differs.

    The command adds a special variable `ci_fairy` to the available jinja2
    variables.
    The unicode character 🧚 can be used in the template if needed instead
    of typing `ci_fairy`.
    """
    if len(root) > 0 and len(root) != len(config):
        raise click.UsageError("--root must be given for each --config")

    if template is None:
        if not config:
            config = [".gitlab-ci/config.yml"]
            template = ".gitlab-ci/ci.template"
            if output_file is None:
                output_file = ".gitlab-ci.yml"
        else:
            raise click.UsageError("--config and template are required")
    elif not config:
        raise click.UsageError("--config and template are required")

    for c in config:
        configfile = Path(c)
        if configfile.name != "-" and not configfile.is_file():
            raise click.BadParameter(
                f"config file {configfile} does not exist or is not a file"
            )
        elif configfile.name == "-" and len(config) > 1:
            raise click.UsageError(
                "stdin is only supported for a single --config option"
            )

    templatefile = Path(template)
    if not templatefile.is_file():
        raise click.BadParameter(
            f"template file {templatefile} does not exist or is not a file"
        )

    data = {}
    for cfile, rootnode in itertools.zip_longest(config, root):
        if cfile == "-":
            cdata = ExtendedYaml.load_from_stream(sys.stdin)
        else:
            cdata = ExtendedYaml.load_from_file(cfile)

        if rootnode:
            try:
                if rootnode.startswith("/"):
                    for r in rootnode[1:].split("/"):
                        cdata = cdata[r]
                else:
                    cdata = cdata[rootnode]
            except KeyError:
                raise click.BadParameter(f"Node {r} not found")

        data.update(cdata)

    if "ci_fairy" in data:
        raise click.BadParameter('"ci_fairy" is a reserved keyword for node names')

    _env_ci_fairy.nodes = dict(data)

    templatedir = templatefile.parent
    env = jinja2.Environment(
        loader=ci_fairyFileSystemLoader(os.fspath(templatedir)),
        trim_blocks=True,
        lstrip_blocks=True,
        keep_trailing_newline=True,
        extensions=["jinja2.ext.do"],
    )
    env.globals["ci_fairy"] = _env_ci_fairy

    # note: the following is for documentation purpose mostly
    # jinja2 doesn't accept random unicode as variable names,
    # so we have to tweak the loader to 'fix' that outrageous
    # situation
    env.globals["🧚"] = _env_ci_fairy

    template = env.get_template(templatefile.name)

    if output_file is None:
        outfile = sys.stdout
        output_file = "stdout"
    else:
        outfile = open(Path(output_file), "r" if verify else "w")

    if verify:
        import difflib

        newfile = template.render(data)
        oldfile = outfile.read()

        # difflib requires a weird format and we may insert line endings
        # converting to that. Jinja2 does not insert trailing
        # newlines for the last line, so even where we differ on the last
        # newline, we'll assume the files are identical.
        #
        # both need to be in format [ 'line1\n', 'line2\n', ...]
        newfile = [f"{l}\n" for l in newfile.split("\n")]
        oldfile = [f"{l}\n" for l in oldfile.split("\n")]

        diff = difflib.unified_diff(
            oldfile, newfile, tofile=f"{output_file} (generated)", fromfile=output_file
        )

        red = ""
        green = ""
        reset = ""
        try:
            if os.isatty(sys.stdout.fileno()):
                red = colored.fg("red")
                green = colored.fg("green")
                reset = colored.attr("reset")
        # pytest replaces sys.stdout and throws an exception on fileno()
        except io.UnsupportedOperation:
            pass

        # diff evaluates to True even where it's empty...
        have_diff = False
        for l in diff:
            have_diff = True
            if l[0] == "-":
                prefix = red
            elif l[0] == "+":
                prefix = green
            else:
                prefix = ""
            print(f"{prefix}{l}{reset}", end="")

        if have_diff:
            sys.exit(1)
    else:
        template.stream(data).dump(outfile)


@ci_fairy.command()
@click.argument("commit-range", type=str, required=False, default="HEAD")
@click.option(
    "--branch",
    type=str,
    help="Check commits compared to this branch "
    "(default: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME if present or main or master",
)
@click.option(
    "--signed-off-by/--no-signed-off-by",
    default=None,
    help="Require a Signed-off-by tag to be (or not be) present",
)
@click.option(
    "--gpg-signed-commit/--no-gpg-signed-commit",
    default=None,
    help="Require GPG signature to be (or not be) present on the commit",
)
@click.option(
    "--textwidth",
    type=int,
    default=80,
    help="Require the commit message subject to be less than N characters wide.",
)
@click.option(
    "--rules-file",
    type=click.File(),
    required=False,
    help="Custom rules definitions in YAML format",
)
@click.option(
    "--guess-default-branch",
    is_flag=True,
    default=False,
    help="Guess the default branch (main if it exists, otherwise master)",
)
@click.option("--junit-xml", help="junit output file to write to")
@click.pass_context
def check_commits(
    ctx,
    commit_range,
    branch,
    signed_off_by,
    gpg_signed_commit,
    textwidth,
    rules_file,
    junit_xml,
    guess_default_branch,
):
    """
    Check a commit range for some properties. Some checks are always
    performed, others can be changed with commandline arguments.

    Where a commit range is given, that range is validated. Otherwise, the
    commit range is "branch..HEAD", where branch is either the given one, or
    $CI_MERGE_REQUEST_TARGET_BRANCH_NAME, or main, or master. When run in the GitLab
    CI environment, the FDO_UPSTREAM_REPO is added and the commit range
    defaults to "upstream-repo/branchname..HEAD".
    """
    import git

    try:
        repo = git.Repo(".", search_parent_directories=True)
    except git.exc.InvalidGitRepositoryError:
        raise click.UsageError("This must be run from within the git repository")

    # if we're running in the CI we add the upstream project
    # as remote 'cifairy' so we can build the commit range appropriately.
    # We don't do this when running locally because we don't want to mess
    # with a user's git repo.
    if os.getenv("CI"):
        upstream = (
            os.getenv("CI_MERGE_REQUEST_PROJECT_PATH")
            or os.getenv("FDO_UPSTREAM_REPO")
            or os.getenv("CI_PROJECT_PATH")
        )
        logger.info(f"Using upstream repo: {upstream}")
        host = os.getenv("CI_SERVER_HOST")
        token = os.getenv("CI_JOB_TOKEN")
        url = f"https://gitlab-ci-token:{token}@{host}/{upstream}"

        jobid = os.getenv("CI_JOB_ID", "")
        remote = f"cifairy{jobid}"
        assert remote not in repo.remotes
        upstream = repo.create_remote(remote, url)
        upstream.fetch()
        remote_prefix = f"{remote}/"
    else:
        remote_prefix = ""

    if not branch and guess_default_branch:
        for b in ["main", "master"]:
            if b in repo.branches:
                branch = b
                break
        else:
            raise click.BadParameter("Unable to guess default branch")

    # a user-specified range takes precedence. if it's just a single commit
    # sha fall back to the target_branch..$sha range
    if ".." not in commit_range:
        sha = commit_range
        target_branch = (
            branch
            or os.getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME")
            or os.getenv("CI_DEFAULT_BRANCH")
        )
        if not target_branch:
            logger.error("Unable to find branch to merge onto")
            sys.exit(1)
        commit_range = f"{remote_prefix}{target_branch}..{sha}"
    logger.debug(f"Using commit range {commit_range}")

    if rules_file is None:
        default_rules = Path(".gitlab-ci/commit-rules.yml")
        if default_rules.exists():
            rules_file = default_rules.open()

    if rules_file is not None:
        yml = os.path.expandvars(rules_file.read())
        rules = yaml.safe_load(yml)
    else:
        rules = None

    failures = []
    for commit in repo.iter_commits(commit_range):
        validator = GitCommitValidator(commit)
        validator.check()
        if signed_off_by is not None:
            validator.check_sob(signed_off_by)
        if gpg_signed_commit is not None:
            validator.check_gpg_signed(gpg_signed_commit)
        if textwidth > 0:
            validator.check_text_width(textwidth)
        if rules is not None:
            validator.check_patterns(rules)

        if validator.failed:
            failures.append(validator)
            logger.error(validator.error)

    if junit_xml and failures:
        sha = str(next(repo.iter_commits("HEAD")))[:8]
        make_junit_xml(
            junit_xml,
            f"commitmsg.{sha}",
            f"Commit message check for {sha}",
            [f.junit_tuple for f in failures],
        )

    sys.exit(1 if failures else 0)


@ci_fairy.command()
@click.option(
    "--project",
    help="Project the merge request is filed against, e.g. freedesktop/ci-templates",
)
@click.option("--merge-request-iid", help="The merge request IID to check")
@click.option("--junit-xml", help="junit output file to write to")
@click.option(
    "--require-allow-collaboration",
    is_flag=True,
    help="Check that allow_collaboration is set",
)
@click.pass_context
def check_merge_request(
    ctx, project, merge_request_iid, junit_xml, require_allow_collaboration
):
    """
    Checks the given merge request in the project for various settings.
    Currently supported:

    --require-allow-collaboration: checks that the ``allow_collaboration``
        boolean flag is set (the one that allows maintainers to rebase an
        MR).

    If the requirements are not set, this commands exit with status code 1.
    If a junit output is given, this command writes a junit XML file with an
    appropriate error message.

    This command defaults to CI_MERGE_REQUEST_PROJECT_ID and CI_MERGE_REQUEST_IID,
    see https://docs.gitlab.com/ce/ci/merge_request_pipelines/index.html

    Where not present, it uses FDO_UPSTREAM_REPO to find the upstream
    repository and finds the merge request associated with this commit. This
    relies on CI_COMMIT_SHA.

    This command exits with status 3 if no suitable merge request could be
    found.
    """
    fallback = None
    if not project:
        fallback = (
            os.getenv("CI_MERGE_REQUEST_PROJECT_ID")
            or os.getenv("FDO_UPSTREAM_REPO")
            or os.getenv("CI_PROJECT_ID")
        )

    p = gitlab_project(ctx.obj.gitlab, project, fallback=fallback)
    if not p:
        raise click.BadParameter("Unable to find or identify project")

    if not require_allow_collaboration:
        raise click.UsageError("At least one check must be specified")

    failures = []
    exit_status = 0

    # First check: CI_MERGE_REQUEST_IID. If we have this one we can use it
    # directly but since that requires a detached pipeline (and rules: in
    # the gitlab-ci.yml file) most projects won't bother, so...
    if merge_request_iid is None:
        merge_request_iid = os.getenv("CI_MERGE_REQUEST_IID")
    if merge_request_iid is not None:
        mr = p.mergerequests.get(merge_request_iid)
        if not mr:
            raise click.BadParameter(
                f"Merge request IID {merge_request_iid} does not exist"
            )

    # ... the fallback is to check FDO_UPSTREAM_REPO for all open merge requests
    # and compare their sha with the sha of our current pipeline (or HEAD if
    # run locally). But a branch may not have an MR, so we exit with a
    # different exit code in that case.
    else:
        logger.debug("This is not a merge pipeline, searching for MR")
        sha = os.getenv("CI_COMMIT_SHA")
        if not sha:
            import git

            try:
                repo = git.Repo(".")
                sha = repo.commit("HEAD").hexsha
            except git.exc.InvalidGitRepositoryError:
                pass
        if not sha:
            raise click.BadParameter(
                "Cannot find git sha, unable to search for merge request"
            )

        mr = next(
            iter(
                [
                    m
                    for m in p.mergerequests.list(state="opened", per_page=100)
                    if m.sha == sha
                ]
            ),
            None,
        )
        # No open MR with our sha? In the pipeline run after merging
        # the MR is already in 'merged' state - too late for our checks.
        if not mr:
            merged = [
                m
                for m in p.mergerequests.list(
                    state="merged", order_by="updated_at", per_page=100
                )
                if m.sha == sha
            ]
            mr = next(iter(merged), None)
            if mr:
                logger.info(
                    f"Merge request !{mr.id} is already merged, skipping checks"
                )
                sys.exit(0)

        if not mr:
            # Not having a merge request *may* be fine so let's use a
            # special exit code.
            exit_status = 3
            tcaseid = "mr_is_filed"
            tcasename = "Check a merge request is filed"
            message = (
                f"No open merge request against {p.path_with_namespace} with sha {sha}"
            )
            failures.append((tcaseid, tcasename, message))
            logger.error(message)

    if exit_status == 0:
        if require_allow_collaboration:
            try:
                if not mr.allow_collaboration:
                    exit_status = 1
                    tcaseid = "allow_collaboration"
                    tcasename = "Check allow_collaboration checkbox is set"
                    message = """
    Error: This merge request does not allow edits from maintainers.

    Please edit the merge request and set the checkbox to
    "Allow commits from members who can merge to the target branch"
    See https://docs.gitlab.com/ce/user/project/merge_requests/allow_collaboration.html"""
                    failures.append((tcaseid, tcasename, message))
                    logger.error(message)
            except AttributeError:
                # filing an MR against master in your own personal repo
                # doesn't have that checkbox
                pass

    if failures:
        if junit_xml is not None:
            suite_id = f"{p.name}.merge_request.{merge_request_iid}"
            suite_name = f"Merge request check ({p.name} !{merge_request_iid})"
            make_junit_xml(junit_xml, suite_id, suite_name, failures)
        sys.exit(exit_status)


@ci_fairy.command()
@click.option("--project", help="Project to check, e.g. freedesktop/ci-templates")
@click.option(
    "--latest", is_flag=True, help="Use the most recent pipeline instead of the sha"
)
@click.option("--sha", help="Use a specific sha instead of HEAD", default="HEAD")
@click.option(
    "--interval", help="The polling interval in seconds", type=int, default=30
)
@click.pass_context
def wait_for_pipeline(ctx, project, sha, latest, interval):
    """
    Waits for a pipeline to complete, printing the status of various jobs on
    the terminal. This tool must be called from within the git repository to
    automatically resolve the pipelines.

    The project defaults to $GITLAB_USER_ID/$basename or $USER/$basename if
    the former isn't set.
    """
    if not project:
        userid = os.environ.get("GITLAB_USER_ID") or os.environ.get("USER")
        if not userid:
            raise click.BadParameter("Unable to find or identify project")

        project = f"{userid}/{Path.cwd().name}"

    p = gitlab_project(ctx.obj.gitlab, project, fallback=None)
    if not p:
        raise click.BadParameter("Unable to find or identify project")

    if latest:
        pipelines = p.pipelines.list(per_page=1)
    else:
        import git

        try:
            # If called from within a git repostory, try to resolve the
            # symbolic ref correctly
            repo = git.Repo(search_parent_directories=True)
            sha = repo.rev_parse(sha)
        except gitdb.exc.BadName:
            raise click.BadParameter(f'Unable to parse sha "{sha}"')
        except git.exc.InvalidGitRepositoryError:
            pass

        pipelines = p.pipelines.list(sha=sha)

    if not pipelines:
        logger.info("No matching pipeline found")
        sys.exit(1)

    if len(pipelines) > 1:
        logger.warning("Multiple pipelines found, using most recent one")

    pipeline = pipelines[0]

    # Rough grouping of the various statuses we have
    pipeline_statuses = {
        "active": [
            "created",
            "waiting_for_resource",
            "preparing",
            "pending",
            "running",
        ],
        "failed": ["failed", "canceled"],
        "complete": ["success", "skipped", "manual", "scheduled"],
    }

    job_statuses = {
        "active": ["created", "pending", "running"],
        "failed": ["failed", "canceled"],
        "complete": ["skipped", "manual", "success"],
    }

    print(f"Pipeline {pipeline.web_url}")
    exit_code = 0
    pulse = 0
    while True:
        pipeline = p.pipelines.get(pipeline.id)

        # Minor race condition: the pipeline may complete while we fetch the
        # jobs below and we'd be printing the wrong status. Too niche to
        # worry about.
        if pipeline.status in pipeline_statuses["failed"]:
            exit_code = 1

        jobs = pipeline.jobs.list(per_page=100)
        active = [j for j in jobs if j.status in job_statuses["active"]]
        njobs = len(jobs)
        ndone = njobs - len(active)

        # We use this dict as histogram for each job status, ignoring
        # skipped, canceled and manual since we don't usually need to
        # monitor these.
        statuses = {
            name: 0 for name in ["created", "pending", "running", "failed", "success"]
        }
        for j in jobs:
            try:
                statuses[j.status] += 1
            except KeyError:
                pass  # not all job statuses are monitored

        # Now assemble the output line
        # \x1B[2K == "delete line"
        output = f"\r\x1B[2K status: {pipeline.status[:9]:9s} | {ndone}/{njobs} "
        for s, count in statuses.items():
            output += f"| {s}: {count:2d} "

        # The pulse is just there to make sure something is visibly moving
        output += "." * (pulse % 5)
        pulse += 1
        print(output, end="", flush=True)

        if not active:
            break

        time.sleep(interval)

    print("")
    sys.exit(exit_code)


def main(*args, **kwargs):
    ci_fairy(*args, **kwargs)


if __name__ == "__main__":
    main()
