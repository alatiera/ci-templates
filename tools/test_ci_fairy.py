#!/usr/bin/env python3

from click.testing import CliRunner
from unittest.mock import patch, MagicMock
import botocore
import datetime
import git
import json
import pytest
from dateutil import parser as date_parser
from pathlib import Path
from textwrap import dedent

import ci_fairy
from ci_fairy import ExtendedYaml, YamlError

GITLAB_TEST_URL = "https://test.gitlab.url"
GITLAB_TEST_PROJECT_ID = "11"
GITLAB_TEST_PROJECT_PATH = "project12/path34"

MINIO_TEST_SERVER = "min.io.url:9000"
MINIO_TEST_URL = "http://min.io.url:9000"

S3_TEST_URL = "https://s3.url"

# A note on @patch('ci_fairy.Gitlab')
# because we use from gitlab import Gitlab, the actual instance sits in
# ci_fairy.Gitlab and we need to patch that instance.


@pytest.fixture
def gitlab_url():
    return GITLAB_TEST_URL


@pytest.fixture
def gitlab_project_path():
    return GITLAB_TEST_PROJECT_PATH


@pytest.fixture
def gitlab_default_env():
    return {
        "CI": "1",
        "CI_SERVER_URL": GITLAB_TEST_URL,
        "CI_PROJECT_PATH": GITLAB_TEST_PROJECT_PATH,
        "CI_PROJECT_ID": GITLAB_TEST_PROJECT_ID,
        "CI_DEFAULT_BRANCH": "main",
        "CI_MERGE_REQUEST_IID": None,
        "CI_MERGE_REQUEST_PROJECT_ID": None,
        "CI_MERGE_REQUEST_TARGET_BRANCH_NAME": None,
    }


def mock_gitlab(gitlab):
    repos = []
    for i in range(3):
        repo = MagicMock()
        repo.name = f"repository/{i}"
        repos.append(repo)

        tags = []
        for t in range(5):
            tag = MagicMock()
            tag.name = f"tag-{t}"
            tags.append(tag)

        repo.tags = MagicMock()
        repo.tags.list = MagicMock(return_value=tags)

    project = MagicMock()
    project.name = GITLAB_TEST_PROJECT_PATH
    project.path = GITLAB_TEST_PROJECT_PATH
    project.path_with_namespace = GITLAB_TEST_PROJECT_PATH
    project.id = GITLAB_TEST_PROJECT_ID
    project.repositories.list = MagicMock(return_value=repos)

    mrs = []
    for i in range(4):
        mr = MagicMock()
        mr.state = "opened" if i < 2 else "merged"
        mr.id = i
        mr.sha = f"dead{i:08x}"
        mr.allow_collaboration = (i % 2) == 0
        mrs.append(mr)

    def mrget(mid):
        return [m for m in mrs if m.id == int(mid)][0]

    def mrlist(state=None, per_page=None, order_by="created_at"):
        # order_by is ignored for our tests
        return [m for m in mrs if state is None or m.state == state]

    project.mergerequests.list = MagicMock(side_effect=mrlist)
    project.mergerequests.get = MagicMock(side_effect=mrget)

    pipelines = []
    for i in range(5):
        p = MagicMock()
        p.status = {0: "running", 1: "failed", 2: "success"}[i % 3]
        p.id = i
        p.sha = f"dead{i:08x}"
        p.web_url = f"{GITLAB_TEST_URL}/-/pipelines/{p.id}"
        pipelines.append(p)

    def plget(pid):
        return [p for p in pipelines if p.id == int(pid)][0]

    def pllist(sha=None, per_page=None, order_by="created_at"):
        # order_by is ignored for our tests
        return [p for p in pipelines if sha is None or p.sha == sha]

    project.pipelines.list = MagicMock(side_effect=pllist)
    project.pipelines.get = MagicMock(side_effect=plget)

    ctx = gitlab(GITLAB_TEST_URL)
    ctx.projects.list = MagicMock(return_value=[project])
    # This always returns our project, even where the ID doesn't match. Good
    # enough for tests but may cause false positives.
    ctx.projects.get = MagicMock(return_value=project)

    return ctx, project, repos


@patch("ci_fairy.Gitlab")
def test_project_lookup(gitlab, caplog, gitlab_default_env, monkeypatch):
    # instantiate here so ci-fairy will instantiate the same objects
    ctx = gitlab(GITLAB_TEST_URL)

    monkeypatch.setenv("CI_PROJECT_ID", "3")

    class Project:
        def __init__(self, name_with_namespace, id):
            _, name = name_with_namespace.split("/")
            self.name = name
            self.path = name
            self.path_with_namespace = name_with_namespace
            self.id = id

    projects = []
    for idx, name in enumerate(["ns/foo", "user/foo", "ns/foobar", "foo/foo"]):
        projects.append(Project(name, idx))

    def pget(pid):
        return [p for p in projects if p.id == int(pid)][0]

    ctx.projects.get = MagicMock(side_effect=pget)

    def plist(search, search_namespaces=True):
        return [p for p in projects if search in p.path_with_namespace]

    ctx.projects.list = MagicMock(side_effect=plist)

    # default is CI_PROJECT_ID
    p = ci_fairy.gitlab_project(ctx)
    assert p is not None
    assert p.name == "foo"
    assert p.path_with_namespace == "foo/foo"
    assert p.id == 3

    # exact match
    p = ci_fairy.gitlab_project(ctx, "user/foo")
    assert p is not None
    assert p.name == "foo"
    assert p.path_with_namespace == "user/foo"
    assert p.id == 1

    # too many matches, not precise enough
    p = ci_fairy.gitlab_project(ctx, "foo")
    assert p is None

    # exact match
    p = ci_fairy.gitlab_project(ctx, "foobar")
    assert p is not None
    assert p.name == "foobar"
    assert p.id == 2

    # exact match
    p = ci_fairy.gitlab_project(ctx, "foo/foo")
    assert p is not None
    assert p.name == "foo"
    assert p.path_with_namespace == "foo/foo"
    assert p.id == 3


def test_missing_url(caplog, gitlab_default_env):
    args = ["delete-image", "--all", "--dry-run"]  # need one subcommand

    env = gitlab_default_env
    env["CI_SERVER_URL"] = ""

    runner = CliRunner(env=env)
    # Must not be run within our git repo, otherwise it'll default to fdo
    with runner.isolated_filesystem():
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 2
        assert "Missing gitlab URL" in result.stdout


@patch("ci_fairy.Gitlab")
def test_url_extractor(gitlab, caplog, gitlab_default_env):
    args = ["delete-image", "--all", "--dry-run"]  # need one subcommand

    env = gitlab_default_env
    env["CI_SERVER_URL"] = ""
    env["CI_JOB_TOKEN"] = ""

    urls = [
        "https://foo.bar/repo.git",
        "http://foo.bar/repo.git",
        "http://user@foo.bar/path/repo.git",
        "user@foo.bar:path/repo.git",
        "ssh://user@foo.bar:path/repo.git",
        "git+ssh://git@foo.bar:path/repo.git",
        "https://foo.bar:path/repo.git",
    ]
    for url in urls:
        runner = CliRunner(env=env)
        # Must not be run within our git repo, otherwise it'll default to fdo
        with runner.isolated_filesystem():
            repo = git.Repo.init()
            repo.create_remote("origin", url)
            runner.invoke(ci_fairy.ci_fairy, args)
            gitlab.assert_called_with("https://foo.bar")


@patch("ci_fairy.Gitlab")
def test_no_auth(gitlab, caplog, gitlab_default_env):
    args = ["delete-image", "--all", "--dry-run"]  # need one subcommand

    env = gitlab_default_env
    env["CI_JOB_TOKEN"] = ""

    runner = CliRunner(env=env)
    runner.invoke(ci_fairy.ci_fairy, args)
    gitlab.assert_called_with(GITLAB_TEST_URL)


@patch("ci_fairy.Gitlab")
def test_job_token_auth(gitlab, caplog, gitlab_default_env):
    token = "tokenval"
    args = ["delete-image", "--all", "--dry-run"]  # need one subcommand
    env = gitlab_default_env.copy()
    env["CI_JOB_TOKEN"] = token
    runner = CliRunner(env=env)
    runner.invoke(ci_fairy.ci_fairy, args)
    gitlab.assert_called_with(GITLAB_TEST_URL, job_token=token)


@patch("ci_fairy.Gitlab")
def test_private_token_auth(gitlab, caplog, gitlab_default_env):
    token = "tokenval"
    authfile = "afile"
    args = [
        "--authfile",
        authfile,
        "delete-image",
        "--all",
        "--dry-run",
    ]  # need one subcommand
    env = gitlab_default_env.copy()
    # --authfile overrides CI_JOB_TOKEN
    env["CI_JOB_TOKEN"] = "123245"
    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        with open(authfile, "w") as fd:
            fd.write(token)
        runner.invoke(ci_fairy.ci_fairy, args)
        gitlab.assert_called_with(GITLAB_TEST_URL, private_token=token)


@patch("ci_fairy.Gitlab")
def test_delete_image_missing_arg(gitlab, caplog, gitlab_default_env):
    args = ["delete-image"]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 2
    assert "One of --tag, --exclude-tag, or --all is required." in result.stdout


@patch("ci_fairy.Gitlab")
def test_delete_image_invalid_project(gitlab, caplog, gitlab_default_env):
    # instantiate here so ci-fairy will instantiate the same objects
    ctx = gitlab(GITLAB_TEST_URL)

    p1 = MagicMock()
    p1.name = "foo"
    p1.id = 0
    p2 = MagicMock()
    p2.name = "foo"
    p2.id = 1

    # empty list or more than one of projects triggers an error
    for rv in [[], [p1, p2]]:
        ctx.projects.list = MagicMock(return_value=rv)
        args = ["delete-image", "--all", "--project", "foo"]
        runner = CliRunner(env=gitlab_default_env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 2
        assert "Unable to find or identify project" in result.stdout


@pytest.mark.parametrize("reponame", [None, "repository-3"])
@pytest.mark.parametrize("dry_run", [True, False])
@patch("ci_fairy.Gitlab")
def test_delete_image_all(gitlab, caplog, gitlab_default_env, reponame, dry_run):
    gitlab, project, repos = mock_gitlab(gitlab)

    args = ["delete-image", "--all"]
    if reponame:
        args += ["--repository", reponame]
    if dry_run:
        args += ["--dry-run"]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if dry_run:
                t.delete.assert_not_called()
            elif reponame is None or reponame == r.name:
                t.delete.assert_called()
            else:
                t.delete.assert_not_called()


@pytest.mark.parametrize("reponame", [None, "repository-2"])
@patch("ci_fairy.Gitlab")
def test_delete_image_exclude_tag(gitlab, caplog, gitlab_default_env, reponame):
    gitlab, project, repos = mock_gitlab(gitlab)

    TAGNAME = "tag-2"

    args = ["delete-image", "--exclude-tag", TAGNAME]
    if reponame:
        args += ["--repository", reponame]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if t.name == TAGNAME or (reponame and reponame != r.name):
                t.delete.assert_not_called()
            else:
                t.delete.assert_called()


@pytest.mark.parametrize("reponame", [None, "repository-1"])
@patch("ci_fairy.Gitlab")
def test_delete_image_tag(gitlab, caplog, gitlab_default_env, reponame):
    gitlab, project, repos = mock_gitlab(gitlab)

    TAGNAME = "tag-1"

    args = ["delete-image", "--tag", TAGNAME]
    if reponame:
        args += ["--repository", reponame]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if (reponame is None or reponame == r.name) and t.name == TAGNAME:
                t.delete.assert_called()
            else:
                t.delete.assert_not_called()


@pytest.mark.parametrize("reponame", [None, "repository-1"])
@patch("ci_fairy.Gitlab")
def test_delete_image_tag_fnmatch(gitlab, caplog, gitlab_default_env, reponame):
    gitlab, project, repos = mock_gitlab(gitlab)

    TAGNAME = "tag-*"

    args = ["delete-image", "--tag", TAGNAME]
    if reponame:
        args += ["--repository", reponame]
    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0

    for r in repos:
        for t in r.tags.list():
            if reponame is None or reponame == r.name:
                t.delete.assert_called()
            else:
                t.delete.assert_not_called()


@pytest.mark.parametrize("yamlfile", [None, "custom.yml"])
@pytest.mark.parametrize("project", [None, "foo/bar"])
@patch("ci_fairy.Gitlab")
def test_lint(gitlab, caplog, gitlab_default_env, yamlfile, project):
    gitlab, *_ = mock_gitlab(gitlab)

    gitlab.lint = MagicMock(return_value=(True, []))

    linter = MagicMock()
    gitlab_project = MagicMock()
    gitlab_project.ci_lint = linter
    gitlab.projects.get.return_value = gitlab_project

    args = ["lint"]
    if yamlfile is not None:
        args += ["--filename", yamlfile]
    else:
        yamlfile = ".gitlab-ci.yml"

    if project:
        args += ["--project", project]
    else:
        project = "freedesktop/ci-templates"

    runner = CliRunner(env=gitlab_default_env)
    with runner.isolated_filesystem():
        with open(yamlfile, "w") as fd:
            fd.write("test: ci")
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        gitlab.projects.get.assert_called_with(project)
        linter.create.assert_called_with({"content": "test: ci"})
        gitlab.lint.assert_not_called()


DEFAULT_YAML = """
numbers:
    one: 1
    two: 2
    three: 3

fruit:
  apple:
    color: green
    health: 78
  banana:
    color: yellow
    health: 52
  orange:
    color: orange
    health: 65
"""

SECOND_YAML = """
animals:
 dog:
   noise: bark
 tiger:
   noise: roar
 shark:
   noise: doo doo doo doo doo doo
"""


def test_template_simple():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("three is {{numbers.three}}")

        args = ["generate-template", "--config", "test.yml", "test.tmpl"]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "three is 3"


def test_template_loop():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("{% for f in fruit %}{{f}}{% endfor %}")

        args = ["generate-template", "--config", "test.yml", "test.tmpl"]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "applebananaorange"


def test_template_defaults():
    runner = CliRunner()
    with runner.isolated_filesystem():
        cidir = Path(".gitlab-ci")
        cidir.mkdir()
        config = cidir / "config.yml"
        template = cidir / "ci.template"
        with open(config, "w") as fd:
            fd.write(DEFAULT_YAML)
        with open(template, "w") as fd:
            fd.write("three is {{numbers.three}}")

        args = ["generate-template"]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        outfile = Path(".gitlab-ci.yml")
        assert outfile.exists()
        assert open(outfile).read() == "three is 3"


@pytest.mark.parametrize("rootnode", ["fruit", "/fruit"])
def test_template_root(rootnode):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("health: {{banana.health}}")

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            rootnode,
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "health: 52"


def test_template_root_path():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("health: {{health}}")

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            "/fruit/orange",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "health: 65"


def test_template_multiple_config():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test1.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test2.yml", "w") as fd:
            fd.write(SECOND_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write('{{animals|join(" ")}} {{fruit|join(" ")}}')

        args = [
            "generate-template",
            "--config",
            "test1.yml",
            "--config",
            "test2.yml",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "dog tiger shark apple banana orange"


def test_template_multiple_config_with_root():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test1.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test2.yml", "w") as fd:
            fd.write(SECOND_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("{{noise}} {{health}}")

        args = [
            "generate-template",
            "--config",
            "test1.yml",
            "--root",
            "/fruit/apple",
            "--config",
            "test2.yml",
            "--root",
            "/animals/tiger",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "roar 78"

        # invalid: either no --root anywhere or matching counts of root and
        # config
        args = [
            "generate-template",
            "--config",
            "test1.yml",
            "--config",
            "test2.yml",
            "--root",
            "/animals/tiger",
            "test.tmpl",
        ]
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 2


@pytest.mark.parametrize("fairy", ["ci_fairy", "🧚"])
def test_template_ci_fairy_hashfiles(fairy):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("filea.txt", "w") as fd:
            fd.write("abc\n")
        with open("test.tmpl", "w") as fd:
            fd.write(f'file1: {{{{ { fairy }.hashfiles("filea.txt") }}}}\n')
            fd.write(f'file2: {{{{ { fairy }.hashfiles("./filea.txt") }}}}\n')
            fd.write(
                f'file3: {{{{ { fairy }.hashfiles("filea.txt", "filea.txt") }}}}\n'
            )
            fd.write(f'file4: {{{{ { fairy }.hashfiles("test.tmpl") }}}}\n')
            fd.write(f'file5: {{{{ { fairy }.hashfiles("./test.tmpl") }}}}\n')
            fd.write(
                f'file6: {{{{ { fairy }.hashfiles("test.tmpl", "test.tmpl") }}}}\n'
            )
            fd.write(
                f'file7: {{{{ { fairy }.hashfiles("filea.txt", "test.tmpl") }}}}\n'
            )

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            "/fruit/orange",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        expected_stdout = (
            "file1: 14b3d22e38217fb8835c1fd799454d20836f99cbd822cf5949f24ff5e3bd2bd5\n"
            + "file2: a8992d7bc5335366e4acba105047e956e10cfb9d7bd836eeffbb7e1fcc7fd809\n"
            + "file3: c17f78cc120f554d3ed42e80c3c9f2eeaaf7418b0f1d963c3e15ebcbdf2365aa\n"
        )

        if fairy == "ci_fairy":
            expected_stdout += (
                "file4: 4a4c1b3cd68939cb8b7934f58bb7c411b10856ad778403f935672d42e44ea3d0\n"
                + "file5: cf578ac3b0bb8de869eb8aec17873e3e4ce28be4df6151af6abbb202b386dc34\n"
                + "file6: e4ab45e5961c17493e504896d35945619bed9c5183aae9b9cd9f028c74320906\n"
                + "file7: dbc23bd9d9e6d52829aefdca4da566620337c2136b7942af85b6e21f664f7684\n"
            )
        else:
            expected_stdout += (
                "file4: 7adb8b5752d1db679351981ee78a436e58ebeb959991c7cecca9ae023962237f\n"
                + "file5: 3f56301c71db500ef96ae3f32538f95d40b6c8ed09f00261c588e85de9e0db76\n"
                + "file6: 5b79358b35b772960ddf8c94efd854300b71f3896f7fa23aa0fecab77276b928\n"
                + "file7: 3627bdca680bb5f490301d5bdf52dfdc17b983b290eba0ba4dd36216f02ca4a0\n"
            )

        assert result.stdout == expected_stdout


@pytest.mark.parametrize("fairy", ["ci_fairy", "🧚"])
def test_template_ci_fairy_sha256sum(fairy):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("filea.txt", "w") as fd:
            fd.write("abc\n")
        with open("test.tmpl", "w") as fd:
            fd.write(f'file1: {{{{ { fairy }.sha256sum("filea.txt") }}}}\n')
            fd.write(f'file2: {{{{ { fairy }.sha256sum("./filea.txt") }}}}\n')
            fd.write(
                f'file3: {{{{ { fairy }.sha256sum("./filea.txt", prefix=True) }}}}\n'
            )
            fd.write(
                f'file4: {{{{ { fairy }.sha256sum("./filea.txt", prefix=False) }}}}\n'
            )

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            "/fruit/orange",
            "test.tmpl",
        ]

        sha = "edeaaff3f1774ad2888673770c6d64097e391bc362d7d6fb34982ddf0efd18cb"

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert (
            result.stdout
            == f"file1: sha256-{sha}\n"
            + f"file2: sha256-{sha}\n"
            + f"file3: sha256-{sha}\n"
            + f"file4: {sha}\n"
        )


@pytest.mark.parametrize("fairy", ["ci_fairy", "🧚"])
def test_template_ci_fairy_nodes(fairy):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write(f"{{% for key, item in { fairy }.nodes.items() %}}\n")
            fd.write("{{ key }}:\n")
            fd.write("  {% for node in item %}\n")
            fd.write("  {{ node }}\n")
            fd.write("  {% endfor %}\n")
            fd.write("{% endfor %}\n")

        args = ["generate-template", "--config", "test.yml", "test.tmpl"]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert (
            result.stdout
            == "numbers:\n"
            + "  one\n"
            + "  two\n"
            + "  three\n"
            + "fruit:\n"
            + "  apple\n"
            + "  banana\n"
            + "  orange\n"
        )


def test_template_ci_fairy_import_module():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("{% set calendar = ci_fairy.import_module('calendar') %}\n")
            fd.write("{{ calendar.timegm((1970,1,1,0,0,0,0,0,0)) }}\n")

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            "/fruit/orange",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "0\n"

        # ensure 'calendar' is not already available as a keyword
        with open("test.tmpl", "w") as fd:
            fd.write("{{ calendar.timegm((1970,1,1,0,0,0,0,0,0)) }}\n")

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            "/fruit/orange",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1


def test_template_ci_fairy_root_nodes():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("{% for key, item in ci_fairy.nodes.items() %}\n")
            fd.write("{{ key }}\n")
            fd.write("{% endfor %}\n")

        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--root",
            "/fruit/orange",
            "test.tmpl",
        ]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert result.stdout == "color\n" + "health\n"


def test_template_no_ci_fairy():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write("ci_fairy: {}")
        with open("test.tmpl", "w") as fd:
            fd.write("this should not work.")

        args = ["generate-template", "--config", "test.yml", "test.tmpl"]

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 2


# Run twice, once with trailing newline in the source file, once without
@pytest.mark.parametrize("last_line", ["line to be removed\n", "line to be removed"])
def test_template_verify(last_line):
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("test.yml", "w") as fd:
            fd.write(DEFAULT_YAML)
        with open("test.tmpl", "w") as fd:
            fd.write("three is {{numbers.three}}\n")
            fd.write(last_line)

        # generate the first file
        args = [
            "generate-template",
            "--config",
            "test.yml",
            "--output-file",
            "outfile",
            "test.tmpl",
        ]
        result = runner.invoke(ci_fairy.ci_fairy, args)
        # File "output" is generated now

        args.append("--verify")

        # check we're happy with identical data
        with open("test.tmpl", "w") as fd:
            fd.write("three is {{numbers.three}}\n")
            fd.write(last_line)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        print(result.stdout)
        assert result.exit_code == 0

        # add a line, remove a line
        with open("test.tmpl", "w") as fd:
            fd.write("three is {{numbers.three}}\n")
            fd.write("added line\n")
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert "-line to be removed" in result.stdout
        assert "+added line" in result.stdout

        # but other whitespace is an error
        with open("test.tmpl", "w") as fd:
            fd.write("three is {{numbers.three}}\n")
            fd.write("line to be removed ")  # extra space
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1

        # but other whitespace is an error
        with open("test.tmpl", "w") as fd:
            fd.write("three is {{numbers.three}}\n")
            fd.write("line to be removed\n\n")  # extra newline
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1


def test_template_extendedyaml_load():
    yml = ExtendedYaml.load_from_stream("foo: bar")
    assert yml is not None


def test_template_extendedyaml_load_invalid():
    with pytest.raises(YamlError):
        ExtendedYaml.load_from_stream("1")

    with pytest.raises(YamlError):
        ExtendedYaml.load_from_stream("[1, 2]")

    with pytest.raises(YamlError):
        ExtendedYaml.load_from_stream("[1, 2]")

    with pytest.raises(YamlError):
        ExtendedYaml.load_from_stream("foo")


def test_template_extendedyaml_key_access():
    yml = ExtendedYaml.load_from_stream("foo: bar\nbaz: bat")
    assert yml["foo"] == "bar"
    assert yml["baz"] == "bat"

    yml = ExtendedYaml.load_from_stream("foo: [1, 2]\nbaz: bat")
    assert yml["foo"] == [1, 2]
    assert yml["baz"] == "bat"

    yml = ExtendedYaml.load_from_stream("foo: [1, 2]\nbaz: 3")
    assert yml["foo"] == [1, 2]
    assert yml["baz"] == 3


def test_template_extendedyaml_iteration():
    data = dedent(
        """\
                  foo: bar
                  baz: bat
                  """
    )
    yml = ExtendedYaml.load_from_stream(data)
    assert [k for k in yml] == ["foo", "baz"]
    assert [(k, v) for (k, v) in yml.items()] == [("foo", "bar"), ("baz", "bat")]


def test_template_extendedyaml_extends():
    data = dedent(
        """\
                  foo:
                    one: two
                  bar:
                    extends: foo
                  """
    )
    yml = ExtendedYaml.load_from_stream(data)
    assert yml["foo"]["one"] == "two"
    assert yml["bar"]["one"] == "two"

    data = dedent(
        """\
                  foo:
                    one: two
                  bar:
                    extends: foo
                    one: three
                  """
    )
    yml = ExtendedYaml.load_from_stream(data)
    assert yml["foo"]["one"] == "two"
    assert yml["bar"]["one"] == "three"

    data = dedent(
        """\
                  foo:
                    one: [1, 2]
                  bar:
                    extends: foo
                    one: [3, 4]
                  """
    )
    yml = ExtendedYaml.load_from_stream(data)
    assert yml["foo"]["one"] == [1, 2]
    assert yml["bar"]["one"] == [1, 2, 3, 4]


def test_template_extendedyaml_extends_invalid():
    data = dedent(
        """\
                  foo:
                    one: [1, 2]
                  extends: bar
                  """
    )
    with pytest.raises(YamlError) as e:
        ExtendedYaml.load_from_stream(data)
        assert "Invalid section name" in e.message

    data = dedent(
        """\
                  foo:
                    one: [1, 2]
                  bar:
                    extends: foobar
                  """
    )
    with pytest.raises(YamlError) as e:
        ExtendedYaml.load_from_stream(data)
        assert "Invalid section" in e.message

    data = dedent(
        """\
                  foo:
                    one: [1, 2]
                  bar:
                    extends: bar
                    one: str
                  """
    )
    with pytest.raises(YamlError) as e:
        ExtendedYaml.load_from_stream(data)
        assert "Mismatched" in e.message


def test_template_extendedyaml_include():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("to-include.yaml", "w") as fd:
            fd.write("version: 1\n")
            fd.write("\n")
            fd.write("one:\n")
            fd.write("    two: three\n")
            fd.write("    four: five\n")
            fd.write("    six: [7, 8]\n")
        with open("include-test.yaml", "w") as fd:
            fd.write("foo:\n")
            fd.write("    bar: baz\n")
            fd.write("\n")
            fd.write("include: to-include.yaml\n")
        yml = ExtendedYaml.load_from_file(Path("include-test.yaml"))
        assert yml["foo"]["bar"] == "baz"
        assert yml["one"]["two"] == "three"


def test_template_extendedyaml_infinite_include():
    runner = CliRunner()
    with runner.isolated_filesystem():
        with open("infinite-include.yaml", "w") as fd:
            fd.write("foo:\n")
            fd.write("    bar: infinite\n")
            fd.write("\n")
            fd.write("include: infinite-include.yaml\n")
        yml = ExtendedYaml.load_from_file("infinite-include.yaml")
        assert yml["foo"]["bar"] == "infinite"


def test_template_extendedyaml_version():
    data = dedent(
        """\
                  foo:
                    one: [1, 2]
                  """
    )
    yml = ExtendedYaml.load_from_stream(data)
    with pytest.raises(AttributeError):
        yml.version

    data = dedent(
        """\
                  version: 1
                  foo:
                    one: [1, 2]
                  """
    )
    yml = ExtendedYaml.load_from_stream(data)
    assert yml.version == 1

    runner = CliRunner()
    with runner.isolated_filesystem():
        with pytest.raises(YamlError) as e:
            with open("to-include.yaml", "w") as fd:
                fd.write("version: 1\n")
                fd.write("\n")
                fd.write("one:\n")
                fd.write("    two: three\n")
                fd.write("    four: five\n")
                fd.write("    six: [7, 8]\n")
            with open("wrong-version.yaml", "w") as fd:
                fd.write("version: 2\n")
                fd.write("include: to-include.yaml\n")
            ExtendedYaml.load_from_file(Path("wrong-version.yaml"))
            assert "version mismatch" in e.message


def test_commits_needs_git_repo(caplog):
    runner = CliRunner()
    with runner.isolated_filesystem():
        args = ["check-commits"]
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 2
        assert "This must be run from within the git repository" in result.stdout


@pytest.mark.parametrize("main_branch", ["main", "master"])
def test_commits_noop(gitlab_default_env, main_branch):
    env = gitlab_default_env
    env["CI"] = ""
    env["CI_DEFAULT_BRANCH"] = main_branch
    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["check-commits"]

        repo = git.Repo.init(**{"initial-branch": main_branch})
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()
        with open("test", "w") as fd:
            fd.write("foo")
            repo.index.add(["test"])
            repo.index.commit("initial commit")

        # nothing to compare here, we're on master
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0


@pytest.mark.parametrize("main_branch", ["main", "master"])
def test_commits_fixup_squash(caplog, gitlab_default_env, main_branch):
    env = gitlab_default_env
    env["CI"] = ""
    env["CI_DEFAULT_BRANCH"] = main_branch

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["check-commits"]

        repo = git.Repo.init(**{"initial-branch": main_branch})
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()
        with open("test", "w") as fd:
            fd.write("foo")
            repo.index.add(["test"])
            repo.index.commit("initial commit")

        b = repo.create_head("mybranch")
        repo.head.reference = b
        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            repo.index.commit("fixup! blah")

            # whack on another commit to verify we're testing the range, not
            # just HEAD
            fd.write("baz")
            repo.index.add(["test"])
            repo.index.commit("normal commit")

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Leftover "fixup!"' in "".join(caplog.messages)

        b = repo.create_head("mybranch2", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            repo.index.commit("squash! blah")

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert 'Leftover "fixup!"' in "".join(caplog.messages)


@pytest.mark.parametrize("main_branch", ["main", "master"])
def test_commits_sob(caplog, gitlab_default_env, main_branch):
    env = gitlab_default_env
    env["CI"] = ""
    env["CI_DEFAULT_BRANCH"] = main_branch

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        repo = git.Repo.init(**{"initial-branch": main_branch})
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()

        with open("test", "w") as fd:
            fd.write("foo")
            repo.index.add(["test"])
            repo.index.commit("initial commit")

        b = repo.create_head("mybranch")
        repo.head.reference = b
        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            repo.index.commit("Lacking a s-o-b")

        args = ["check-commits"]
        result = runner.invoke(ci_fairy.ci_fairy, args + ["--no-signed-off-by"])
        assert result.exit_code == 0

        result = runner.invoke(ci_fairy.ci_fairy, args + ["--signed-off-by"])
        assert result.exit_code == 1
        assert 'Missing "Signed-off-by: author information"' in "".join(caplog.messages)

        b = repo.create_head("mybranch2", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            repo.index.commit("Commit\n\nSigned-off-by: first last <email>")

        result = runner.invoke(ci_fairy.ci_fairy, args + ["--no-signed-off-by"])
        assert result.exit_code == 1
        assert "Do not use Signed-off-by in commits" in "".join(caplog.messages)

        result = runner.invoke(ci_fairy.ci_fairy, args + ["--signed-off-by"])
        assert result.exit_code == 0

        # Signed-off-by: must be at the beginning of the line
        b = repo.create_head("mybranch3", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            repo.index.commit("Commit\n\n___Signed-off-by: first last <email>")

        result = runner.invoke(ci_fairy.ci_fairy, args + ["--no-signed-off-by"])
        assert result.exit_code == 0

        result = runner.invoke(ci_fairy.ci_fairy, args + ["--signed-off-by"])
        assert result.exit_code == 1
        assert 'Missing "Signed-off-by: author information"' in "".join(caplog.messages)


FORMAT_RULES = """
patterns:
  deny:
    - regex: '^[^:]*: [a-z]'
      message: Commit message subject should be properly Capitalized.
      where: subject
"""


@pytest.mark.parametrize("main_branch", ["main", "master"])
def test_commits_msgformat(caplog, gitlab_default_env, main_branch):
    env = gitlab_default_env
    env["CI"] = ""
    env["CI_DEFAULT_BRANCH"] = main_branch

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["check-commits"]

        repo = git.Repo.init(**{"initial-branch": main_branch})
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()

        with open("test", "w") as fd:
            fd.write("foo")
        repo.index.add(["test"])
        repo.index.commit("initial commit")

        b = repo.create_head("mybranch")
        repo.head.reference = b
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        # second line must be empty
        repo.index.commit("three\nline\ncommit")

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert "Second line in commit message must be empty" in "".join(caplog.messages)

        b = repo.create_head("mybranch2", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        repo.index.commit("too long a line" * 10)

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert "Commit message subject must not exceed" in "".join(caplog.messages)

        result = runner.invoke(ci_fairy.ci_fairy, args + ["--textwidth=1000"])
        assert result.exit_code == 0

        b = repo.create_head("mybranch3", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            # anything other than the subject can exceed the textwidth
            repo.index.commit("short-enough subject\n\ntoo long a line" * 10)

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        # Fake the revert here because ci-fairy runs against the previous
        # commits too. If we have a commit with a long line and revert that
        # we would pass the revert commit but still fail on the original
        # commit. Easiest to just fake a revert commit ourselves.
        b = repo.create_head("mybranch4", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        msg = """Revert "one two three one two three one two three one two three one two three"

        This reverts commit 123456
        """
        repo.index.commit(msg)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        # Revert line may have a longer line than allowed
        assert result.exit_code == 0

        b = repo.create_head("mybranch5", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        repo.index.commit("foo: bar")

        result = runner.invoke(
            ci_fairy.ci_fairy, args + ["--rules-file", "-"], input=FORMAT_RULES
        )
        assert result.exit_code == 1
        assert "Commit message subject should be properly Capitalized." in "".join(
            caplog.messages
        )

        b = repo.create_head("mybranch6", main_branch)
        repo.head.reference = b
        repo.head.reset(index=True, working_tree=True)
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        repo.index.commit("foo: Bar\n\nfoo: bar")

        result = runner.invoke(
            ci_fairy.ci_fairy, args + ["--rules-file", "-"], input=FORMAT_RULES
        )
        assert result.exit_code == 0


@pytest.mark.parametrize("main_branch", ["main", "master"])
def test_commits_gitlabemail(caplog, gitlab_default_env, main_branch):
    env = gitlab_default_env
    env["CI"] = ""
    env["CI_DEFAULT_BRANCH"] = main_branch

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["check-commits"]

        repo = git.Repo.init(**{"initial-branch": main_branch})
        config = repo.config_writer()
        config.set_value(
            "user", "email", "testuser@users.noreply.gitlab.freedesktop.org"
        )
        config.set_value("user", "name", "test user")
        config.release()

        with open("test", "w") as fd:
            fd.write("foo")
            repo.index.add(["test"])
            repo.index.commit("initial commit")

        b = repo.create_head("mybranch")
        repo.head.reference = b
        with open("test", "w") as fd:
            fd.write("bar")
            repo.index.add(["test"])
            repo.index.commit("second commit")

        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert "git author email invalid" in "".join(caplog.messages)


def test_commits_prefer_main_deleted_master(caplog, gitlab_default_env):
    env = gitlab_default_env
    env["CI"] = ""

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["check-commits"]

        repo = git.Repo.init(initial_branch="master")
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()

        # Repo structure will be:
        #
        # initial commit - broken commit - ok commit - test-commit
        #   [master]                        [main]     [mybranch]
        #
        # So that master..mybranch would trigger an error but main..mybranch
        # won't
        with open("test", "w") as fd:
            fd.write("foo")
        repo.index.add(["test"])
        repo.index.commit("initial commit")

        repo.head.reference = repo.create_head("tmp")
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        # second line must be empty
        repo.index.commit("broken\ncommit\ncommit")

        with open("test", "w") as fd:
            fd.write("baz")
        repo.index.add(["test"])
        # second line must be empty
        repo.index.commit("ok\n\ncommit")

        # now create the main branch on the ok commit
        main = repo.create_head("main")
        repo.head.reference = main

        b = repo.create_head("mybranch")
        repo.head.reference = b
        with open("test", "w") as fd:
            fd.write("bat")
        repo.index.add(["test"])
        repo.index.commit("test\n\ncommit\n")

        # With no arguments, default to main
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        args += ["--branch=master"]
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert "Second line in commit message must be empty" in "".join(caplog.messages)


def test_commits_prefer_main_deleted_master(caplog, gitlab_default_env):
    env = gitlab_default_env
    env["CI"] = ""

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["check-commits"]

        repo = git.Repo.init(initial_branch="master")
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()

        # Repo structure will be:
        #

        # initial commit - broken commit - ok commit - test-commit
        #   [master]                        [main]     [mybranch]
        #
        # So that master..mybranch would trigger an error but main..mybranch
        # won't
        with open("test", "w") as fd:
            fd.write("foo")
        repo.index.add(["test"])
        repo.index.commit("initial commit")

        repo.head.reference = repo.create_head("tmp")
        with open("test", "w") as fd:
            fd.write("bar")
        repo.index.add(["test"])
        # second line must be empty
        repo.index.commit("broken\ncommit\ncommit")

        with open("test", "w") as fd:
            fd.write("baz")
        repo.index.add(["test"])
        # second line must be empty
        repo.index.commit("ok\n\ncommit")

        # now create the main branch on the ok commit
        main = repo.create_head("main")
        repo.head.reference = main

        b = repo.create_head("mybranch")
        repo.head.reference = b
        with open("test", "w") as fd:
            fd.write("bat")
        repo.index.add(["test"])
        repo.index.commit("test\n\ncommit\n")

        # Expect main
        args += ["--guess-default-branch"]
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        # Delete main, expect master to get picked
        repo.delete_head("main")
        args += ["--guess-default-branch"]
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert "Second line in commit message must be empty" in "".join(caplog.messages)


@pytest.mark.parametrize(
    "upstream",
    (
        ("CI_MERGE_REQUEST_PROJECT_PATH", "foo/bar"),
        ("FDO_UPSTREAM_REPO", "fdo/upstream"),
        ("CI_PROJECT_PATH", "ci/project"),
    ),
)
def test_commits_upstream_selection(caplog, gitlab_default_env, upstream):
    token = "tokenval"
    host = "example.com"

    env = gitlab_default_env
    env["CI_MERGE_REQUEST_PROJECT_PATH"] = ""
    env["FDO_UPSTREAM_REPO"] = ""
    env["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"] = "target-branch"
    env["CI_JOB_TOKEN"] = token
    env["CI_JOB_ID"] = "123"
    env["CI_SERVER_HOST"] = host
    env[upstream[0]] = upstream[1]

    runner = CliRunner(env=env)
    with runner.isolated_filesystem():
        args = ["--verbose", "check-commits"]

        repo = git.Repo.init(**{"initial-branch": "main"})
        config = repo.config_writer()
        config.set_value("user", "email", "test@example.com")
        config.set_value("user", "name", "test user")
        config.release()

        with open("test", "w") as fd:
            fd.write("foo")
        repo.index.add(["test"])
        repo.index.commit("initial commit")

        with patch("git.Repo") as repo:
            repo.return_value = repo
            runner.invoke(ci_fairy.ci_fairy, args)
            repo.create_remote.assert_called_with(
                "cifairy123", f"https://gitlab-ci-token:{token}@{host}/{upstream[1]}"
            )
            assert f"Using upstream repo: {upstream[1]}" in "".join(caplog.messages)


@patch("ci_fairy.Gitlab")
def test_merge_request_missing_arg(gitlab, caplog, gitlab_default_env):
    args = ["check-merge-request"]

    runner = CliRunner(env=gitlab_default_env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 2
    assert "At least one check must be specified" in result.stdout


@patch("ci_fairy.Gitlab")
def test_merge_request_detached_pipeline(gitlab, caplog, gitlab_default_env):
    args = ["check-merge-request", "--require-allow-collaboration"]

    env = gitlab_default_env
    env["CI_MERGE_REQUEST_PROJECT_ID"] = GITLAB_TEST_PROJECT_ID
    env["CI_MERGE_REQUEST_IID"] = "1"

    # mock_gitlab sets allow_collaboration off for every second MR
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 1
    assert "This merge request does not allow edits from maintainers" in caplog.text

    env["CI_MERGE_REQUEST_IID"] = "0"
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0


@patch("ci_fairy.Gitlab")
def test_merge_request_fdo_upstream_repo(gitlab, caplog, gitlab_default_env):
    args = ["-vv", "check-merge-request", "--require-allow-collaboration"]

    env = gitlab_default_env
    env["FDO_UPSTREAM_REPO"] = GITLAB_TEST_PROJECT_ID
    env["CI_PROJECT_ID"] = "9999"
    env["CI_COMMIT_SHA"] = "dead00000001"

    # mock_gitlab sets allow_collaboration off for every second MR
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 1
    assert "This merge request does not allow edits from maintainers" in caplog.text

    env["CI_COMMIT_SHA"] = "dead00000000"
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0


@patch("ci_fairy.Gitlab")
def test_merge_request_already_merged(gitlab, caplog, gitlab_default_env):
    args = ["-vv", "check-merge-request", "--require-allow-collaboration"]

    env = gitlab_default_env
    env["FDO_UPSTREAM_REPO"] = GITLAB_TEST_PROJECT_ID
    env["CI_PROJECT_ID"] = "9999"
    env["CI_COMMIT_SHA"] = "dead00000002"  # MRs 2 and 3 are 'merged'

    # mock_gitlab sets allow_collaboration off for every second MR
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0
    assert "Merge request !2 is already merged, skipping checks" in caplog.text

    env["CI_COMMIT_SHA"] = "dead00000003"
    gitlab, project, _ = mock_gitlab(gitlab)
    runner = CliRunner(env=env)
    result = runner.invoke(ci_fairy.ci_fairy, args)
    assert result.exit_code == 0
    assert "Merge request !3 is already merged, skipping checks" in caplog.text


# fun fact: @patch is a stack, so the order of arguments matters
@patch("ci_fairy.Path.cwd", MagicMock(return_value=Path(GITLAB_TEST_PROJECT_PATH)))
@patch("ci_fairy.Gitlab")
def test_wait_for_pipeline(gitlab, caplog, gitlab_default_env):
    args = ["-vv", "wait-for-pipeline"]

    env = gitlab_default_env
    env["USER"] = "project12"

    git = MagicMock()

    with patch.dict("sys.modules", git=git):
        # a minimal git repository, we need this for the sha lookup
        def mock_gitrepo(git, sha):
            repo = MagicMock()
            repo.rev_parse = MagicMock(return_value=sha)
            git.Repo = MagicMock(return_value=repo)

        # See mock pipelines setup, they're in order
        # running/failed/success for each
        mock_gitrepo(git, "dead00000000")
        gitlab, project, _ = mock_gitlab(gitlab)
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert f'status: {"running":9s}' in result.output

        args = ["-vv", "wait-for-pipeline", "--sha=dead00000001"]
        mock_gitrepo(git, "dead00000001")
        gitlab, project, _ = mock_gitlab(gitlab)
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 1
        assert f'status: {"failed":9s}' in result.output

        args = ["-vv", "wait-for-pipeline", "--sha=dead00000002"]
        mock_gitrepo(git, "dead00000002")
        gitlab, project, _ = mock_gitlab(gitlab)
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert f'status: {"success":9s}' in result.output

        args = ["-vv", "wait-for-pipeline", "--latest"]
        mock_gitrepo(git, "dead00000003")
        gitlab, project, _ = mock_gitlab(gitlab)
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert f'status: {"running":9s}' in result.output

        # Basically just an argparse check - because we don't set up
        # the jobs in the pipeline we never actually invoke the
        # interval. Something for later maybe
        args = ["-vv", "wait-for-pipeline", "--interval=5"]
        mock_gitrepo(git, "dead00000003")
        gitlab, project, _ = mock_gitlab(gitlab)
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert f'status: {"running":9s}' in result.output

        # Fallback to GITLAB_USER_ID
        # we can't remove USER because it's in the os.env, but setting it to the
        # empty string is enough for it to be False
        env["USER"] = ""
        env["GITLAB_USER_ID"] = "project12"
        args = ["-vv", "wait-for-pipeline"]
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0

        # we can't remove GITLAB_USER_ID because it'll be set in the
        # pipeline, but setting it to the empty string is enough for
        # it to be False
        env["USER"] = ""
        env["GITLAB_USER_ID"] = ""
        args = ["-vv", "wait-for-pipeline"]
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 2
        assert "Unable to find or identify project" in result.stdout

        args = ["-vv", "wait-for-pipeline", f"--project={GITLAB_TEST_PROJECT_PATH}"]
        mock_gitrepo(git, "dead00000003")
        gitlab, project, _ = mock_gitlab(gitlab)
        runner = CliRunner(env=env)
        result = runner.invoke(ci_fairy.ci_fairy, args)
        assert result.exit_code == 0
        assert f'status: {"running":9s}' in result.output


def mock_s3_session(session):
    client = MagicMock(name="client")
    assume_role_with_web_identity = MagicMock(name="assume_role_with_web_identity")

    ctx = session()
    ctx.client = client
    ctx.assume_role_with_web_identity = assume_role_with_web_identity

    return ctx


@patch("json.dump")
@patch("ci_fairy.boto3.Session")
def test_minio_login(session, json_dump, caplog):
    args = ["-vv", "minio", "login", "--endpoint-url", MINIO_TEST_URL, "1234"]

    # check that the endpoint we provide is actually used by the botocore client
    ctx = mock_s3_session(session)
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

        assert result.exit_code == 0

        # python 3.8 allows to have `mock.call_args.kwargs`, but python 3.7
        # doesn't. So use the 3.7 version (tuple access) until 3.8 gets
        # more widespread.
        assert ctx.client.call_args[1]["endpoint_url"] == MINIO_TEST_URL

        sts = ctx.client.return_value
        assert (
            sts.assume_role_with_web_identity.call_args[1]["WebIdentityToken"] == "1234"
        )

    # ensure that if the caller forgets about the token ('1234' above), we fail
    args = ["-vv", "minio", "login", "--endpoint-url", MINIO_TEST_URL]
    with runner.isolated_filesystem():
        result = runner.invoke(ci_fairy.ci_fairy, args)

        assert result.exit_code == 2

    tokenfile = "token"
    args = [
        "-vv",
        "minio",
        "login",
        "--endpoint-url",
        MINIO_TEST_URL,
        "--token-file",
        tokenfile,
    ]
    with runner.isolated_filesystem():
        with open(tokenfile, "w") as f:
            f.write("5678")

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=True)

        assert result.exit_code == 0

        assert ctx.client.call_args[1]["endpoint_url"] == MINIO_TEST_URL

        sts = ctx.client.return_value
        assert (
            sts.assume_role_with_web_identity.call_args[1]["WebIdentityToken"] == "5678"
        )


def mock_minio(minio):
    error_response = {"Error": {"Code": 404, "Message": "Not Found"}}
    _404 = botocore.exceptions.ClientError(error_response, "HeadObject")
    buckets = []
    for i in range(3):
        bucket = MagicMock()
        bucket.name = f"bucket{i}"
        buckets.append(bucket)

        files = []
        f = MagicMock()
        f.key = "root_file.txt"
        files.append(f)
        for _dir in range(5):
            for _f in range(4):
                f = MagicMock()
                f.key = f"dir-{_dir}/file-{_f}"
                files.append(f)

        def upload_file(src, dst):
            f = MagicMock()
            f.key = dst
            files.append(f)

        def download_file(src, dst):
            if src not in [f.key for f in files]:
                raise _404
            with open(dst, "w") as f:
                f.write("Hello World!")

        def list_objects():
            return files

        bucket.objects.all.side_effect = list_objects
        bucket.upload_file.side_effect = upload_file
        bucket.download_file.side_effect = download_file

    ctx = minio(endpoint_url=MINIO_TEST_URL)
    ctx.buckets.all = MagicMock(return_value=buckets)

    def bucket_side_effect(arg):
        for b in buckets:
            if b.name == arg:
                return b
        bucket = MagicMock()
        bucket.name = arg
        bucket.objects.all = MagicMock(return_value=[])

        bucket.upload_file.side_effect = _404
        bucket.download_file.side_effect = _404
        return bucket

    ctx.Bucket.side_effect = bucket_side_effect

    return ctx, buckets


def write_minio_credentials():
    with open(".minio_credentials", "w") as f:
        json.dump(
            {
                MINIO_TEST_SERVER: {
                    "endpoint_url": MINIO_TEST_URL,
                    "AccessKeyId": "1234",
                    "SecretAccessKey": "5678",
                    "SessionToken": "9101112",
                }
            },
            f,
        )


@patch("ci_fairy.boto3.resource")
@pytest.mark.parametrize(
    "input_path,result_files",
    [
        # fmt: off
        (None, ['hello.txt', '.minio_credentials']),
        ('.', ['hello.txt', '.minio_credentials']),
        ('minio:', None),
        ('minio:/', None),
        ('minio://', None),
        (f'minio://{MINIO_TEST_SERVER}', [f'bucket{i}' for i in range(3)]),
        (f'minio://{MINIO_TEST_SERVER}/', [f'bucket{i}' for i in range(3)]),
        ('minio://WRONG_MINIO_TEST_SERVER', None),
        ('minio://WRONG_MINIO_TEST_SERVER/bucket/path', None),
        (f'minio://{MINIO_TEST_SERVER}/bucket1', ['root_file.txt'] + [f'dir-{i}' for i in range(5)]),
        (f'minio://{MINIO_TEST_SERVER}/bucket0/', ['root_file.txt'] + [f'dir-{i}' for i in range(5)]),
        (f'minio://{MINIO_TEST_SERVER}/non_existant_bucket', None),
        (f'minio://{MINIO_TEST_SERVER}/bucket0/non_existent_dir_or_file', None),
        (f'minio://{MINIO_TEST_SERVER}/bucket2/dir-2', [f'file-{i}' for i in range(4)]),
        (f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/', [f'file-{i}' for i in range(4)]),
        (f'minio://{MINIO_TEST_SERVER}/bucket2/dir-0/file-3', ['file-3']),
        # fmt: on
    ],
)
def test_minio_ls(minio, input_path, result_files, caplog):
    runner = CliRunner()

    args = ["minio", "ls"]

    if input_path is not None:
        args.append(input_path)

    ctx, buckets = mock_minio(minio)

    with runner.isolated_filesystem():
        write_minio_credentials()

        with open("hello.txt", "w") as f:
            f.write("Hello World!")

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

        if result_files is None:
            assert result.exit_code == 2
            prefix = "minio://"
            error_msg = result.output.strip().split("\n")[-1]

            if input_path.startswith(prefix):
                input_path = input_path[len(prefix) :]

            if input_path.startswith("WRONG_MINIO_TEST_SERVER"):
                input_path = "WRONG_MINIO_TEST_SERVER"
            elif input_path.startswith(MINIO_TEST_SERVER):
                input_path = MINIO_TEST_SERVER

            assert input_path in error_msg

        else:
            assert result.exit_code == 0

            output = result.output.strip("\n").split("\n")
            assert set(result_files) == set(output)


@patch("ci_fairy.boto3.resource")
def test_minio_ls_no_creds(minio, caplog):
    runner = CliRunner()

    args = ["minio", "ls"]

    ctx, buckets = mock_minio(minio)

    with runner.isolated_filesystem():
        with open("hello.txt", "w") as f:
            f.write("Hello World!")

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

    assert result.exit_code == 2


@patch("ci_fairy.boto3.resource")
@pytest.mark.parametrize(
    "input_args, expected_error",
    [
        # fmt: off
        #######################################################
        # working cases
        #######################################################

        # copy a local file
        (['hello.txt', 'an_other_local_file'], None),

        # copy a local file to a dir
        (['hello.txt', 'emptydir/'], None),

        # copy a local file to a dir
        (['hello.txt', 'emptydir/an_other_local_file'], None),

        # download an existing distant file
        ([f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/file-2', 'result'], None),

        # download an existing distant file on an existing dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/file-2', 'emptydir/'], None),

        # download an existing distant file on an existing dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/file-2', 'emptydir/result'], None),

        # download an existing distant file on an existing dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-2/file-3', 'emptydir//result'], None),

        # download an existing distant file on an existing dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket2/dir-0/file-2', 'emptydir///result'], None),

        # upload a local file on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/'], None),

        # upload a local file on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/hello2.txt'], None),

        # upload a local file on a new dir on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket1/new_dir/'], None),

        # upload a local file on a new dir on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/new_dir/hello2.txt'], None),

        # upload a local file on an existing dir on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/'], None),

        # upload a local file on an existing dir on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0/dir-1/hello2.txt'], None),

        # upload a local file on an existing dir on an existing bucket
        (['emptydir/../hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket2/dir-3/'], None),

        # upload a local file on an existing dir on an existing bucket
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/bucket0//dir-1/hello2.txt'], None),

        #######################################################
        # failures
        #######################################################

        # click error: not enough arguments
        ([], 'Error: Missing argument'),

        # click error: not enough arguments
        (['local_file'], 'Error: Missing argument'),

        # click error: too many arguments
        (['local_file_1', 'local_file_2', 'local_file_3'], 'Error: Got unexpected extra argument (local_file_3)'),

        # source file does not exist
        (['local_file', 'an_other_local_file'], "Error: source file 'local_file' does not exist"),

        # source file does not exist
        ([f'minio://{MINIO_TEST_SERVER}/bucket_that_does_not_exist/file', 'an_other_local_file'], "404"),

        # source file does not exist
        ([f'minio://{MINIO_TEST_SERVER}/bucket1/file', 'an_other_local_file'], "404"),

        # source file is a local dir
        (['.', 'local_file'], 'Error:'),  # looks like Fedora 34 now sends a different error

        # source file is a remote dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket0/', 'local_file'], 'Error:'),  # looks like Fedora 34 now sends a different error

        # source file is a remote dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-0', 'local_file'], '404'),

        # source file is a remote dir
        ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-1/', 'local_file'], 'Error: cannot do recursive cp'),

        # source file is a local empty dir
        (['emptydir', 'local_file'], 'Error:'),  # looks like Fedora 34 now sends a different error

        # source and destination file are remote
        ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-0/file-3', f'minio://{MINIO_TEST_SERVER}/bucket2/an_other_local_file'], 'Error: at least one argument must be a local path'),

        # destination is invalid (no host)
        (['hello.txt', 'minio://'], 'Error: "missing host information in \'minio://\'"'),

        # destination is invalid (no bucket)
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/'], 'Error: No destination bucket provided'),

        # destination is invalid (wrong host)
        (['hello.txt', 'minio://WRONG_MINIO_TEST_SERVER/'], 'Error: "host \'WRONG_MINIO_TEST_SERVER\' not found in credentials'),

        # destination is invalid (no bucket)
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}//dir-0/'], '404'),

        # destination is invalid (wrong bucket)
        (['hello.txt', f'minio://{MINIO_TEST_SERVER}/this_bucket_doesnt_exist/dir-0/'], '404'),

        # destination is invalid (new dir)
        ([f'minio://{MINIO_TEST_SERVER}/bucket1/dir-0/file-1', 'new_dir/'], "Error: directory 'new_dir' does not exist"),
        # fmt: on
    ],
)
def test_minio_cp(minio, input_args, expected_error, caplog):
    runner = CliRunner()

    default_args = ["minio", "cp"]

    args = default_args + input_args

    ctx, buckets = mock_minio(minio)

    with runner.isolated_filesystem():
        write_minio_credentials()

        with open("hello.txt", "w") as f:
            f.write("Hello World!")

        Path("emptydir").mkdir()

        result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

        if expected_error is None:
            assert result.exit_code == 0

            check_file = input_args[1]
            if check_file.endswith("/"):
                check_file += Path(input_args[0]).name

            args = ["minio", "ls", check_file]
            check_result = runner.invoke(
                ci_fairy.ci_fairy, args, catch_exceptions=False
            )

            assert check_result.exit_code == 0

            if check_file.startswith("minio://"):
                check_file = check_file[len("minio://") :]

            path = Path(check_file)

            assert path.name in check_result.output

        else:
            assert result.exit_code == 2
            error_msg = result.output.strip().split("\n")[-1]

            assert expected_error in error_msg


def mock_s3(req):
    files = []
    f = MagicMock()
    f.key = "/public/root_file.txt"
    files.append(f)
    for _dir in range(5):
        for _f in range(4):
            f = MagicMock()
            f.key = f"/public/dir-{_dir}/file-{_f}"
            files.append(f)
            f = MagicMock()
            f.key = f"/private/dir-{_dir}/file-{_f}"
            files.append(f)

    def has_jwt(kwargs):
        headers = kwargs["headers"]
        return (
            "Authorization" in headers
            and headers["Authorization"] == "Bearer 0123456789abcdef"
        )

    def validate_expires(kwargs):
        headers = kwargs["headers"]
        if "Expires" not in headers:
            return True
        expires = headers["Expires"]
        expired = date_parser.parse(expires)

        days = (expired - datetime.datetime.now() + datetime.timedelta(minutes=2)).days
        return days == 3

    def req_side_effect(arg):
        r = MagicMock()
        if not arg.startswith(S3_TEST_URL):
            raise Exception(f"calling requests on a non test url: {arg}")

        path = arg[len(S3_TEST_URL) :]

        bucket = Path(path.lstrip("/")).parts[0]
        return r, path, bucket

    def get_side_effect(url, **kwargs):
        r, path, bucket = req_side_effect(url)

        if bucket == "private" and not has_jwt(kwargs):
            r.status_code = 403
            r.content = b""
            return r

        if path not in [f.key for f in files]:
            r.status_code = 404
            r.content = '<?xml version="1.0" encoding="UTF-8"?><Error><Code>NoSuchKey</Code><BucketName>artifacts</BucketName><RequestId>tx000002334d568b67ad11d-0063480f52-2c14108-fdo-opa</RequestId><HostId>2c14108-fdo-opa-fdo-opa</HostId></Error>'
            return r

        r.status_code = 200
        r.content = b"Hello World!"
        return r

    def put_side_effect(url, **kwargs):
        r, path, bucket = req_side_effect(url)

        if not has_jwt(kwargs):
            r.status_code = 403
            r.content = b""
            return r

        if not validate_expires(kwargs):
            r.status_code = 500
            r.content = b"wrong input expires parameter, expected '3d'"
            return r

        f = MagicMock()
        f.key = path
        files.append(f)

        r.status_code = 200
        r.content = b""
        return r

    req.get.side_effect = get_side_effect
    req.put.side_effect = put_side_effect

    return req, files


@patch("ci_fairy.requests")
@pytest.mark.parametrize(
    "input_args, expected_error, use_token",
    [
        # fmt: off
        #######################################################
        # working cases
        #######################################################

        # download an existing distant file
        ([f'{S3_TEST_URL}/public/dir-1/file-2', 'result'], None, False),

        # download an existing distant file on a private repo
        ([f'{S3_TEST_URL}/private/dir-1/file-2', 'result'], None, True),

        # cat an existing distant file
        ([f'{S3_TEST_URL}/public/dir-1/file-2', '-'], None, False),

        # cat an existing distant file on a private repo
        ([f'{S3_TEST_URL}/private/dir-1/file-2', '-'], None, True),

        # download an existing distant file on an existing dir
        ([f'{S3_TEST_URL}/public/dir-1/file-3', 'emptydir'], None, False),

        # download an existing distant file on an existing dir
        ([f'{S3_TEST_URL}/public/dir-1/file-2', 'emptydir/result'], None, False),

        # download an existing distant file on an existing dir
        ([f'{S3_TEST_URL}/public/dir-2/file-3', 'emptydir//result'], None, False),

        # download an existing distant file on an existing dir
        ([f'{S3_TEST_URL}/public/dir-0/file-2', 'emptydir///result'], None, False),

        # upload a local file to an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt'], None, True),

        # upload a local file on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/private/hello2.txt'], None, True),

        # upload a local file on a new dir on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/private/new_dir/hello2.txt'], None, True),

        # expiration date
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt', '--expires', '3d'], None, True),

        #######################################################
        # failures
        #######################################################

        # download an existing distant file on a private repo
        ([f'{S3_TEST_URL}/private/dir-1/file-2', 'result'], "Error: error 403:", False),

        # upload a local file to an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/public/hello.txt'], "Error: error 403: b''", False),

        # upload a local file on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/public/hello2.txt'], "Error: error 403: b''", False),

        # upload a local file on a new dir on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/public/new_dir/hello2.txt'], "Error: error 403: b''", False),

        # upload a local file to an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt'], "Error: error 403: b''", False),

        # upload a local file on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/private/hello2.txt'], "Error: error 403: b''", False),

        # upload a local file on a new dir on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/private/new_dir/hello2.txt'], "Error: error 403: b''", False),

        # click error: not enough arguments
        ([], 'Error: Missing argument', False),

        # click error: not enough arguments
        (['local_file'], 'Error: Missing argument', False),

        # click error: too many arguments
        (['local_file_1', 'local_file_2', 'local_file_3'], 'Error: Got unexpected extra argument (local_file_3)', False),

        # source file does not exist
        (['local_file', f'{S3_TEST_URL}/public/an_other_local_file'], "Error: source file 'local_file' does not exist", False),

        # copy a local file
        (['hello.txt', 'an_other_local_file'], "Error: src and dst can not be both local paths", False),

        # source and destination file are remote
        ([f'{S3_TEST_URL}/hello.txt', f'{S3_TEST_URL}/an_other_local_file'], "Error: src and dst can not be both remote URLs", False),

        # copy a remote file to a non-existing dir
        ([f'{S3_TEST_URL}/hello.txt', 'newdir/'], "Error: destination directory 'newdir/' does not exist", False),

        # copy a remote file to a non-existing dir with specific filename
        ([f'{S3_TEST_URL}/hello.txt', 'newdir/hello.txt'], "Error: destination directory 'newdir' does not exist", False),

        # upload a local directory to an existing bucket
        (['emptydir', f'{S3_TEST_URL}/public/hello.txt'], "Error: source file 'emptydir' is a directory", False),

        # upload a local directory to an existing bucket
        (['.', f'{S3_TEST_URL}/public/hello.txt'], "Error: source file '.' is a directory", False),

        # upload a local file on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/public/'], "Error: destination file 'https://s3.url/public/' is a directory", False),

        # upload a local file on a new dir on an existing bucket
        (['hello.txt', f'{S3_TEST_URL}/public/new_dir/'], "Error: destination file 'https://s3.url/public/new_dir/' is a directory", False),

        # source file is a remote dir
        ([f'{S3_TEST_URL}/public/', 'local_file'], '404', False),

        # source file is a remote dir
        ([f'{S3_TEST_URL}/public/dir-0', 'local_file'], '404', False),

        # valid expiration date, but check if our test works
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt', '--expires', '4d'], 'Error: error 500: b"wrong input expires parameter, expected \'3d\'"', True),

        # valid expiration date, but check if our test works
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt', '--expires', '42w'], 'Error: error 500: b"wrong input expires parameter, expected \'3d\'"', True),

        # invalid expiration date
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt', '--expires', 'not_a_date'], "Error: invalid --expires parameter, expected the following regex: '(\\d+)([dw])'", True),

        # invalid expiration date
        (['hello.txt', f'{S3_TEST_URL}/private/hello.txt', '--expires', '42 d'], "Error: invalid --expires parameter, expected the following regex: '(\\d+)([dw])'", True),
        # fmt: on
    ],
)
def test_s3cp(req, input_args, expected_error, use_token, caplog):
    runner = CliRunner()

    default_args = ["s3cp"]

    ctx, files = mock_s3(req.Session())

    def run_test(token_plain, token_file):
        with runner.isolated_filesystem():
            with open("hello.txt", "w") as f:
                f.write("Hello World!")

            Path("emptydir").mkdir()

            token = "0123456789abcdef"
            with open("token_file", "w") as f:
                f.write(token)

            args = [a for a in default_args]

            if token_plain:
                args += ["--token", token]

            if token_file:
                args += ["--token-file", "token_file"]

            args += input_args

            result = runner.invoke(ci_fairy.ci_fairy, args, catch_exceptions=False)

            if expected_error is None:
                if result.exit_code != 0:
                    print(result.output.strip().split("\n")[-1])

                assert result.exit_code == 0

                check_file = input_args[1]

                if check_file.startswith(S3_TEST_URL):
                    # we uploaded a file, ensure it is on the server
                    check_file = check_file[len(S3_TEST_URL) :]
                    assert check_file in [f.key for f in files]
                else:
                    path = Path(check_file)
                    # we downloaded a file
                    if check_file == "-":
                        # stdout
                        # ideally we want the following, but it doesn't work, sys.stdout.buffer is not captured:
                        # assert result.output.strip().split('\n')[-1] == 'Hello World!'
                        assert not path.exists()
                    else:
                        # ensure it is on the local disk
                        if path.is_dir():
                            # if the destination is a directory, we get the name from the source
                            path = path / Path(input_args[0]).name
                        assert path.exists()

            else:
                assert result.exit_code == 2
                error_msg = result.output.strip().split("\n")[-1]

                assert expected_error in error_msg

    if use_token:
        # each test is run twice, with or without the token_file
        run_test(True, False)
        run_test(False, True)
    else:
        run_test(False, False)
